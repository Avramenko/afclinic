<?php /* Smarty version Smarty-3.1.5, created on 2012-05-15 21:31:15
         compiled from "/home/creek/www/j.warelab.net/tpl/afclinic/html/module.clients.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10190434094fb2a0f335a097-56681827%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '92354bea7ca210b5b050e60a446abe5194ed938c' => 
    array (
      0 => '/home/creek/www/j.warelab.net/tpl/afclinic/html/module.clients.tpl',
      1 => 1335724878,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10190434094fb2a0f335a097-56681827',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'adate' => 0,
    'dt' => 0,
    'word' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4fb2a0f355094',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4fb2a0f355094')) {function content_4fb2a0f355094($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/creek/www/j.warelab.net/inc/smarty/plugins/modifier.date_format.php';
?><div style="display: none">
    
    <!-- USER -->
    
    <div id="clients-register" class="modal" style="">
        <div id="clients-register-error" class="error"></div>
        <form>
        <h2><?php echo $_smarty_tpl->getConfigVariable('registration');?>
</h2>
        <div><?php echo $_smarty_tpl->getConfigVariable('name');?>
: <input type="text" name="name" value="" /></div>
        <div>E-mail: <input type="text" name="email" value="" /></div>
        <div><?php echo $_smarty_tpl->getConfigVariable('phone');?>
: <input type="text" name="phone" value="" /></div>
        <div>
            <input id="clients-register-button"
                   class="button" type="button" name="save" value="<?php echo $_smarty_tpl->getConfigVariable('registration');?>
" />
            <input class="button" type="reset" name="reset" value="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
" />
        </div>
        </form>
    </div>
    
    <div id="clients-forget" class="modal" style="">
        <div id="clients-forget-error" class="error"></div>
        <form>
        <h2><?php echo $_smarty_tpl->getConfigVariable('forget_password');?>
</h2>
        <div>E-mail: <input type="text" name="email" value="" /></div>
        <div>
            <input id="clients-forget-button"
                   class="button" type="button" name="save" value="<?php echo $_smarty_tpl->getConfigVariable('forget');?>
" />
            <input class="button" type="reset" name="reset" value="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
" />
        </div>
        </form>
    </div>

    <div id="clients-change" class="modal" style="">
        <div id="clients-change-error" class="error"></div>
        <form>
        <h2><?php echo $_smarty_tpl->getConfigVariable('change_password');?>
</h2>
        <div><?php echo $_smarty_tpl->getConfigVariable('password');?>
: <input type="password" name="pass" value="" /></div>
        <div><?php echo $_smarty_tpl->getConfigVariable('repeat');?>
: <input type="password" name="pass2" value="" /></div>
        <div>
            <input id="clients-change-button"
                   class="button" type="button" name="save" value="<?php echo $_smarty_tpl->getConfigVariable('change');?>
" />
            <input class="button" type="reset" name="reset" value="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
" />
        </div>
        </form>
    </div>
    
    <!-- CART -->
    
    <div id="tmp-cart-container" class="modal">
        <h2><?php echo $_smarty_tpl->getConfigVariable('shopcart');?>
</h2>
        <div class="tabl-title">
            <div style="width: 498px;">Наименование</div>
            <div style="width: 70px;">Цена</div>
            <div style="width: 110px;">Кол-во</div>
            <div style="width: 90px;">Стоимость</div>
        </div>
    
        <div class="clear"></div>
    
        <ul id="cart-container">
        </ul>
        
        <div class="all-sum">Итого: <span>0,00 грн.</span></div>

        <form>
        <div class="cart-order-desc">
            <b>Имя:</b>
            <div  style="margin-bottom: 10px;"><input id="cart-name" name="name"></div>
            <b>Телефон:</b>
            <div  style="margin-bottom: 10px;"><input id="cart-phone" name="phone"></div>
            <b>Адрес:</b>
            <div style="margin-bottom: 10px;"><input id="cart-adress" name="adress"></div>
            <div class="clear"></div>
        </div>
        
        <div class="cart-order-desc"><b>Время доставки:</b>
            <div>
                <select name="date" style="width: 200px;">
                    <?php  $_smarty_tpl->tpl_vars['dt'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['dt']->_loop = false;
 $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['adate']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['dt']->key => $_smarty_tpl->tpl_vars['dt']->value){
$_smarty_tpl->tpl_vars['dt']->_loop = true;
 $_smarty_tpl->tpl_vars['word']->value = $_smarty_tpl->tpl_vars['dt']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['dt']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['word']->value;?>
 (<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['dt']->value,"%d.%m.%Y");?>
)</option>
                    <?php } ?>
                </select>
                &nbsp;
                <select name="hour" style="width: 70px;">
                    <option value="00">00</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                </select>
                :
                <select name="minute" style="width: 70px;">
                    <option value="00">00</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">50</option>
                </select>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    
    
    
        <div class="button forms" >
            <input type="button" class="button" id="cart-next" value="<?php echo $_smarty_tpl->getConfigVariable('order');?>
" />
            &nbsp;
            <input type="button" class="button" id="cart-clear" value="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
" />
        </div>
        </form>
        <div clas="clear"></div>
    </div>



    <div id="tmp-cart-item">
        <li id="p%id%" title="%name%">
            <div class="cart-wares">
                <span class="ware-name" >%name%</span>
                <span class="ware-price" >%price%</span>
                <span class="addnumb" >
                    <span>%count%</span>
                    <span class="plus" title="Прибавить">+</span>
                    <span class="minus" title="Отнять">-</span>
                </span>
                <span class="ware-sum">%summ%</span>
                <span class="del" title="Удалить из лукошка">&nbsp;</span>
            </div>
        </li>
    </div>

    <div id="tmp-cart-item2">
        <li id="P%id%" title="%name%">
            <span class="p-name">%name%</span>
            <span class="p-count">%count%шт.</span>
            <span class="p-sum">%summ2%</span>
        </li>
    </div>

</div><?php }} ?>
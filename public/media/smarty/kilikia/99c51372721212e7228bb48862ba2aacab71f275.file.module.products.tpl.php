<?php /* Smarty version Smarty-3.1.5, created on 2012-04-30 10:58:06
         compiled from "/home/creek/www/j.warelab.net/tpl/kilikia/html/module.products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14308368174f9a47a5e26bc1-07928668%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99c51372721212e7228bb48862ba2aacab71f275' => 
    array (
      0 => '/home/creek/www/j.warelab.net/tpl/kilikia/html/module.products.tpl',
      1 => 1335772683,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14308368174f9a47a5e26bc1-07928668',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4f9a47a6080c2',
  'variables' => 
  array (
    'p' => 0,
    'array' => 0,
    'prod' => 0,
    'imgext' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4f9a47a6080c2')) {function content_4f9a47a6080c2($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['p']->value['type']!='product'){?>

<div class="products">
    <?php  $_smarty_tpl->tpl_vars['prod'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['prod']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['prod']->key => $_smarty_tpl->tpl_vars['prod']->value){
$_smarty_tpl->tpl_vars['prod']->_loop = true;
?>
    <div id="p<?php echo $_smarty_tpl->tpl_vars['prod']->value['id'];?>
" class="product">
        <div class="head-name" ><a href="<?php echo @PATH_ROOT;?>
product/<?php echo $_smarty_tpl->tpl_vars['prod']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['prod']->value['name'];?>
</a></div>
        <div class="thumb">
            <a href="<?php echo @PATH_ROOT;?>
media/images/<?php echo $_smarty_tpl->tpl_vars['prod']->value['cover']['id'];?>
.<?php echo $_smarty_tpl->tpl_vars['imgext']->value;?>
" rel="gallery" title="<?php echo $_smarty_tpl->tpl_vars['prod']->value['name'];?>
">
                <img src="<?php echo @PATH_ROOT;?>
media/ithumbs/<?php echo $_smarty_tpl->tpl_vars['prod']->value['cover']['id'];?>
.<?php echo $_smarty_tpl->tpl_vars['imgext']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['prod']->value['name'];?>
"/>
            </a>
            <div class="price"><?php echo $_smarty_tpl->tpl_vars['prod']->value['price'];?>
р. </div>
        </div>
        <div class="text"><?php echo $_smarty_tpl->tpl_vars['prod']->value['text'];?>
</div>
        <div class="add">
            <div class="add-num">
                <div class="add-num-plus"></div>
                <div class="add-number">1</div>
                <div class="add-num-minus"></div>
            </div>
            <div class="add-order"></div>
        </div>
    </div>
    <?php } ?>
    <div class="clear"></div>
</div>

<?php if ($_smarty_tpl->tpl_vars['p']->value['products_pg']){?>
<div class="pagelist content">
    <?php  $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['page']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['p']->value['products_pg']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['page']->key => $_smarty_tpl->tpl_vars['page']->value){
$_smarty_tpl->tpl_vars['page']->_loop = true;
?>
    <a href="?<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</a>
    <?php } ?>
</div>
<?php }?>

<?php }else{ ?>

<div id="p<?php echo $_smarty_tpl->tpl_vars['p']->value['content']['id'];?>
" class="productone">
    <div class="thumb">
        <img src="<?php echo @PATH_ROOT;?>
media/images/<?php echo $_smarty_tpl->tpl_vars['p']->value['content']['cover']['id'];?>
.<?php echo $_smarty_tpl->tpl_vars['imgext']->value;?>
"/>
    </div>
    <div class="desc">
        <h1><?php echo $_smarty_tpl->tpl_vars['p']->value['content']['tName'];?>
</h1>
        <div class="price" title="<?php echo $_smarty_tpl->getConfigVariable('price');?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value['content']['nPrice'];?>
 р.</div>
        <div class="text">
            <b>Описание:</b>
            <hr>
            <?php echo $_smarty_tpl->tpl_vars['p']->value['content']['tDesc'];?>

        </div>
        <div class="clear"></div>
        <div class="text"><?php echo $_smarty_tpl->tpl_vars['p']->value['content']['tText'];?>
</div>
    </div>



</div>

<script>$(document).ready(function(){$(".product > a").colorbox({loop: true});});</script>

<?php }?><?php }} ?>
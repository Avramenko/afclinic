<?php /* Smarty version Smarty-3.1.5, created on 2012-05-17 21:39:36
         compiled from "/home/creek/www/k.warelab.net/tpl/admin/html/tmp.products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9216497744fb545e80c3761-57519935%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9461d0942dc64bcfb4484d6e75e79e21f6655fbe' => 
    array (
      0 => '/home/creek/www/k.warelab.net/tpl/admin/html/tmp.products.tpl',
      1 => 1329938920,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9216497744fb545e80c3761-57519935',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bClass' => 0,
    'PATH' => 0,
    'CLS' => 0,
    'VIEW' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4fb545e82f224',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4fb545e82f224')) {function content_4fb545e82f224($_smarty_tpl) {?><!--products sort list-->
<div style="display: none;">

    <div id="tmp-left-panel-products">
        <div id="products-filter">
            <div>
                <button><?php echo $_smarty_tpl->getConfigVariable('clear_all');?>
</button>
            </div>
            <div id="products-name">
                <span><?php echo $_smarty_tpl->getConfigVariable('name_');?>
</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
"></span>
                <input name="tName" />
            </div>
            <div id="products-text">
                <span><?php echo $_smarty_tpl->getConfigVariable('description');?>
</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
"></span>
                <input name="tText"/>
            </div>
            <div id="products-price-from">
                <span><?php echo $_smarty_tpl->getConfigVariable('from');?>
 (<?php echo $_smarty_tpl->getConfigVariable('price');?>
)</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
"></span>
                <input name="tFrom"/>
            </div>
            <div id="products-price-to">
                <span><?php echo $_smarty_tpl->getConfigVariable('to');?>
 (<?php echo $_smarty_tpl->getConfigVariable('price');?>
)</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
"></span>
                <input name="tTo"/>
            </div>
        </div>

        <br/>

        <div id="products-info">
            <div style="text-align: center">
                <img id="products-info-img" width="125" src="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['img'];?>
noimage.gif" />
            </div>
            <span><?php echo $_smarty_tpl->getConfigVariable('name_');?>
:</span>
            <div id="products-info-tName"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('price');?>
:</span>
            <div id="products-info-nPrice"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('extra');?>
:</span>
            <div id="products-info-tDesc"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('date_start');?>
:</span>
            <div id="products-info-dStart"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('date_last');?>
:</span>
            <div id="products-info-dLast"></div>
        </div>
    </div>

    <div id="tmp-sort-root-item-products">
        <div class="sorting " field="fView"  style="margin-right: 87px; width: 15px;"></div>
        <div class="sorting DESC" field="dLast"  style="margin-right: 6px; width: 60px;"><?php echo $_smarty_tpl->getConfigVariable('date');?>
</div>
        <div class="sorting "     field="nPrice" style="margin-right: 6px; width: 60px;"><?php echo $_smarty_tpl->getConfigVariable('price');?>
</div>
        <div class="sorting "     field="tName"  style="margin-right: 6px; width: 200px;"><?php echo $_smarty_tpl->getConfigVariable('name');?>
</div>
    </div>


    <div id="tmp-sort-products-item">
        <li id="g%id%" title="%name%" class="menusItem %hide%">
            <div>
                <span style="width: 230px;" class="top%top%">%name2%</span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-closethick" title="<?php echo $_smarty_tpl->getConfigVariable('delete');?>
"></span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-pencil" title="<?php echo $_smarty_tpl->getConfigVariable('edit');?>
"></span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-arrowrefresh-1-w" title="<?php echo $_smarty_tpl->getConfigVariable('update_time');?>
"></span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-image" title="<?php echo $_smarty_tpl->getConfigVariable('images');?>
"></span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['CLS']->value['hide'];?>
 <?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 %hideSpan%" title="%hideTitle%"></span>
                <span style="float: right; margin-right: 5px;">%date%</span>
                <span style="float: right; margin-right: 9px;">%price%</span>
            </div>
        </li>
    </div>

    <div id="tmp-dialog-products">
        <table title="%title%">
            <tr>
                <td colspan="3" class="error">&nbsp;</td>
            </tr>
            <tr>
                <td width="100"><?php echo $_smarty_tpl->getConfigVariable('name');?>
</td>
                <td><input name="name" style="width:300px;" value="%name%" /></td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['VIEW']->value['products']['updatetime']){?>
                    <?php echo $_smarty_tpl->getConfigVariable('update_time');?>

                    <input type="radio" name="update_time" value="0" checked="checked" /> <?php echo $_smarty_tpl->getConfigVariable('no');?>

                    <input type="radio" name="update_time" value="1" /> <?php echo $_smarty_tpl->getConfigVariable('yes');?>

                    <?php }?>
                </td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('price');?>
</td>
                <td><input name="price" style="width:300px;" value="%price%" /></td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['VIEW']->value['products']['discount']){?>
                    <?php echo $_smarty_tpl->getConfigVariable('discount');?>
: <input type="text" style="width:70px;" name="discount" value="%discount%" />
                    <?php }?>
                </td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('extra');?>
</td>
                <td><input name="desc" style="width:300px;" value="%desc%" /></td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['VIEW']->value['products']['recomended']){?>
                    <?php echo $_smarty_tpl->getConfigVariable('recomend');?>
: <input type="checkbox" name="recomend" /><br/>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('category');?>
</td>
                <td>
                    <select id="%catId%" name="categoty" style="min-width:200px;"></select>
                </td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['VIEW']->value['products']['store']){?>
                    <button id="%storeId%"><?php echo $_smarty_tpl->getConfigVariable('product_store');?>
</button>
                    <?php }?>
                </td>
            </tr>
            <tr>
                <td colspan="3"><?php echo $_smarty_tpl->getConfigVariable('description');?>
</td>
            </tr>
            <tr>
                <td colspan="3"><textarea id="%textId%" style="width:700px;height: 400px;">%text%</textarea></td>
            </tr>
        </table>
    </div>

    <div id="tmp-dialog-products-store">
        <div id="tab-%id%" title="<?php echo $_smarty_tpl->getConfigVariable('product_store');?>
">
            <ul class="tpanel">
                <li><a href="#in-%id%"><?php echo $_smarty_tpl->getConfigVariable('income');?>
</a></li>
                <li><a href="#bal-%id%"><?php echo $_smarty_tpl->getConfigVariable('balance');?>
</a></li>
            </ul>
            <div class="content">
                <div id="in-%id%" class="tab" style="padding: 5px;">
                    <div>
                        <?php echo $_smarty_tpl->getConfigVariable('size');?>
 <input style="width: 50px;margin-right: 10px;">
                        <?php echo $_smarty_tpl->getConfigVariable('count');?>
 <input style="width: 50px;margin-right: 10px;">
                        <button><?php echo $_smarty_tpl->getConfigVariable('insert');?>
</button>
                    </div>
                    <ul class="simplelist"></ul>
                    <div class="pagelist"></div>
                </div>
                <div id="bal-%id%" class="tab">
                    <ul class="simplelist"></ul>
                </div>
            </div>
        </div>
    </div>

    <div id="tmp-dialog-products-store-list-title">
        <li>
            <span style="width: 120px"><?php echo $_smarty_tpl->getConfigVariable('date');?>
</span>
            <span style="width: 80px"><?php echo $_smarty_tpl->getConfigVariable('size');?>
</span>
            <span style="width: 80px"><?php echo $_smarty_tpl->getConfigVariable('count');?>
</span>
            <span style="width: 100px"></span>
        </li>
    </div>

    <div id="tmp-dialog-products-store-list-elem">
        <li>
            <span style="width: 120px">%date%</span>
            <span style="width: 80px">%name%</span>
            <span style="width: 80px">%count%</span>
            <span style="width: 100px">%edit%</span>
        </li>
    </div>

    <div id="tmp-dialog-products-store-balance-title">
        <li>
            <span style="width: 120px"><?php echo $_smarty_tpl->getConfigVariable('size');?>
</span>
            <span style="width: 80px"><?php echo $_smarty_tpl->getConfigVariable('income');?>
</span>
            <span style="width: 80px"><?php echo $_smarty_tpl->getConfigVariable('outcome');?>
</span>
            <span style="width: 60px"><?php echo $_smarty_tpl->getConfigVariable('balance');?>
</span>
        </li>
    </div>

    <div id="tmp-dialog-products-store-balance-elem">
        <li>
            <span style="width: 120px">%name%</span>
            <span style="width: 80px">%in%</span>
            <span style="width: 80px">%out%</span>
            <span style="width: 60px">%balance%</span>
        </li>
    </div>



</div>
<?php }} ?>
<?php $_config_vars = array (
  'sections' => 
  array (
    'login_page' => 
    array (
      'vars' => 
      array (
        'log_width' => 400,
        'log_mtable_border' => 0,
        'log_mtable_width' => 700,
        'log_title' => 'Вход в панель администрирования',
        'log_login' => 'Пользователь',
        'log_password' => 'Пароль',
        'log_button' => 'Войти',
        'log_invalid' => 'Ощибка входа.<br>Неверное имя пользователя и/или пароль.',
      ),
    ),
    'admin_page' => 
    array (
      'vars' => 
      array (
        'adm_toogle_tree' => 'Открыть/закрыть вложенные страницы',
        'adm_add_page' => 'Добавить страницу',
        'adm_up_sort' => 'Поднять в списке',
        'adm_down_sort' => 'Опустить в списке',
        'adm_trash' => 'Корзина',
        'adm_pistures' => 'Изображения',
        'adm_files' => 'Файлы',
        'adm_options' => 'Опции',
        'adm_logout' => 'Выйти',
        'adm_settings' => 'Настройки',
        'adm_menus' => 'Меню',
        'adm_inserts' => 'Вставки',
        'adm_items' => 'Страница',
        'adm_links' => 'Ссылки',
        'adm_images' => 'Изображения',
        'adm_content' => 'Содержимое',
        'adm_selected_links' => 'Выбранные ссылки',
        'adm_link_list' => 'Список ссылок',
      ),
    ),
  ),
  'vars' => 
  array (
    'dfShort' => '%d %b %Y',
    'dfMedium' => '%d %b %Y, %H:%M',
    'dfFull' => '%d %b %Y, %H:%M:%S',
    'imgExt' => '*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG|*.jpg;*.JPG|*.gif;*.GIF|*.png;*.PNG',
    'imgDesc' => 'Картинки(*.jpg;*.gif;*.png)|JPG файлы(*.jpg)|PNG файлы(*.png)|GIF файлы(*.gif)',
    'fileExt' => '*.pdf;*.doc;*.xls;*.zip;*.rar|*.pdf|*.doc;*.xls|*.zip;*.rar',
    'fileDesc' => 'Файлы(*.pdf;*.doc;*.xls;*.zip;*.rar)|PDF файлы(*.pdf)|Документы MS Office(*.doc;*.xls)|Архивы(*.zip;*.rar)',
    'add' => 'Добавить',
    'address' => 'Адресс',
    'alias' => 'Соответствие',
    'balance' => 'Баланс',
    'cancel' => 'Отменить',
    'caption' => 'Подпись',
    'category' => 'Категория',
    'change' => 'Изменить',
    'clear' => 'Очистить',
    'code' => 'Код',
    'confirm' => 'Подтвердить',
    'confirmed' => 'Подтверждено',
    'comment' => 'Комментарий',
    'comments' => 'Комментарии',
    'count' => 'Количество',
    'date' => 'Дата',
    'delete' => 'Удалить',
    'description' => 'Описание',
    'discount' => 'Скидка',
    'do' => 'Выполнить',
    'done' => 'Выполнено',
    'download' => 'Скачать',
    'edit' => 'Редактировать',
    'extra' => 'Дополнительно',
    'forget' => 'Восстановить',
    'from' => 'От',
    'hide' => 'Скрыть',
    'images' => 'Изображения',
    'income' => 'Приход',
    'insert' => 'Внести',
    'link' => 'Ссылка',
    'login' => 'Логин',
    'metadesc' => 'META описание',
    'metakey' => 'META ключевые слова',
    'name' => 'Имя',
    'name_' => 'Название',
    'news' => 'Новости/Отзывы',
    'no' => 'Нет',
    'order' => 'Заказaть',
    'orders' => 'Заказы',
    'outcome' => 'Расход',
    'password' => 'Пароль',
    'phone' => 'Телефон',
    'price' => 'Цена',
    'products' => 'Товары',
    'product_store' => 'Учет товара',
    'recomend' => 'Рекомендовать',
    'register' => 'Зарегистрировать',
    'registration' => 'Регистрация',
    'remove' => 'Переместить в корзину',
    'repeat' => 'Повторить',
    'restore' => 'Восстановить',
    'root' => 'Корневая директория',
    'save' => 'Сохранить',
    'select' => 'Выбрать',
    'size' => 'Размер',
    'shopcart' => 'Корзина',
    'show' => 'Показать',
    'status' => 'Состояние',
    'sum' => 'Сумма',
    'template' => 'Шаблон',
    'text' => 'Текст',
    'title' => 'Заголовок',
    'to' => 'До',
    'unconfirmed' => 'Неподтверждено',
    'upload' => 'Загрузить',
    'visibility' => 'Скрыть/Показать',
    'view' => 'Просмотреть',
    'yes' => 'Да',
    'change_profile' => 'Изменение профайла',
    'change_password' => 'Изменение пароля',
    'forget_password' => 'Восстановление пароля',
    'clear_all' => 'Очистить всё',
    'date_start' => 'Дата добавления',
    'date_last' => 'Дата активности',
    'update_time' => 'Обновить время',
    'edit_images' => 'Редактировать изображения',
    'edit_files' => 'Редактировать файлы',
    'select_item' => 'Выберите страницу',
    'clear_fields' => 'Очистить поля',
    'code_ins' => 'Код вставки',
    'has_children' => 'У страницы есть дочерние елементы.',
    'not_load_trash' => 'Список корзиный не загружен',
    'delete_all' => 'Удалить со всеми зависимыми елементами',
    'save_editing' => 'Остались не сохранённые изменения.
Сохранить?',
    'conflict_field' => 'Сохранение не возможно.
Присутствуют конфликты с базой!
Исправьте выделенные поля!',
    'empty_field' => 'Присутствуют пустые поля!',
    'no_all_full' => 'Не все поля заполненны!',
    'leave_admin' => 'Покинуть админ панель?',
    'dbl_link' => 'Повторяющаяся ссылка',
    'del_link' => 'Удалить ссылку?',
    'adding_link' => 'Добавление ссылки',
    'editing_link' => 'Редактирование ссылки',
    'editing_code' => 'Редактировать вставку',
    'adding_code' => 'Добавить вставку',
    'del_code' => 'Удалить вставку?',
    'editing_image' => 'Редактировать картинку',
    'adding_image' => 'Добавить картинку',
    'del_image' => 'Удалить картинку?',
    'editing_file' => 'Редактировать файл',
    'adding_file' => 'Добавить файл',
    'del_file' => 'Удалить файл?',
    'editing_client' => 'Редактировать клиента',
    'adding_client' => 'Добавить клиента',
    'del_client' => 'Удалить клиента?',
    'editing_order' => 'Редактировать заказ',
    'adding_order' => 'Добавить заказ',
    'del_order' => 'Удалить заказ?',
    'editing_news' => 'Редактировать Новости/Отзывы',
    'adding_news' => 'Добавить Новости/Отзывы',
    'del_news' => 'Удалить Новости/Отзывы?',
    'editing_product' => 'Редактировать товар',
    'adding_product' => 'Добавить товар',
    'del_product' => 'Удалить товар?',
    'editing_comment' => 'Редактировать комментарий',
    'adding_comment' => 'Добавить комментарий',
    'del_comment' => 'Удалить комментарий?',
    'log_title' => 'Панель администрирования',
    'bad_ext' => 'Не правильное расширение файла',
    'full_lim_tread' => 'Очередь загрузки полна.
 Максимальное количество выбранных файлов -',
    'sets_admins' => 'Системные',
    'sets_items' => 'Страницы',
    'sets_links' => 'Ссылки',
    'sets_menus' => 'Меню',
    'sets_codes' => 'Вставки',
    'sets_images' => 'Изображения',
    'sets_files' => 'Файлы',
    'sets_news' => 'Новости/Отзывы',
    'sets_products' => 'Продукты',
    'sets_clients' => 'Клиенты',
    'sets_orders' => 'Заказы',
    'menus_top' => 'Верхнее',
    'menus_left' => 'Левое',
    'menus_right' => 'Правое',
    'menus_bottom' => 'Нижнее',
    'menus_announce' => 'Верхний анонс',
    'codes_top' => 'Верхние',
    'codes_right' => 'Правые',
    'codes_bottom' => 'Нижние',
    'codes_left' => 'Левые',
    'images_cover' => 'Обложка',
    'images_page' => 'Для описания',
    'images_list' => 'Галлерея',
    'images_slider' => 'Слайдер',
    'files_shared' => 'Общедоступные',
    'files_logged' => 'Для зарегистрированных',
    'files_own' => 'Личные',
  ),
); ?>
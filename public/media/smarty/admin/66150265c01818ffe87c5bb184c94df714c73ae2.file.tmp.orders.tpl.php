<?php /* Smarty version Smarty-3.1.5, created on 2012-07-16 15:48:58
         compiled from "/home/a/afclinic/public_html/tpl/admin/html/tmp.orders.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4454868985003ffaadfe219-56671452%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '66150265c01818ffe87c5bb184c94df714c73ae2' => 
    array (
      0 => '/home/a/afclinic/public_html/tpl/admin/html/tmp.orders.tpl',
      1 => 1337471957,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4454868985003ffaadfe219-56671452',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bClass' => 0,
    'CLS' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_5003ffab153e5',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5003ffab153e5')) {function content_5003ffab153e5($_smarty_tpl) {?><!--orders sort list-->
<div style="display: none;">

    <div id="tmp-left-panel-orders">
        <div id="orders-filter">
            <div id="orders-view">
                <select name="fView">
                    <option value="hidden&shown">Астивные заказы</option>
                    <option value="hidden">Неподтверждённые</option>
                    <option value="shown">Подтверждённые</option>
                    <option value="done">Выполненные</option>
                    <option value="all">Все заказы</option>
                </select>
            </div>
            <div>
                <button><?php echo $_smarty_tpl->getConfigVariable('clear_all');?>
</button>
            </div>
            <div id="orders-desc">
                <span><?php echo $_smarty_tpl->getConfigVariable('address');?>
</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
"></span>
                <input name="tDesc"/>
            </div>
            <div id="orders-name">
                <span><?php echo $_smarty_tpl->getConfigVariable('name');?>
</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('clear');?>
"></span>
                <input name="tName" />
            </div>
            <div id="orders-phone">
                <span><?php echo $_smarty_tpl->getConfigVariable('phone');?>
</span><span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-refresh" title="<?php echo $_smarty_tpl->getConfigVariable('phone');?>
"></span>
                <input name="tPhone"/>
            </div>
        </div>

        <br/>

        <div id="orders-info">
            <span><?php echo $_smarty_tpl->getConfigVariable('address');?>
:</span>
            <div id="orders-info-tDesc"></div>
            <span>Время доставки:</span>
            <div id="orders-info-dLast"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('phone');?>
:</span>
            <div id="orders-info-tPhone"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('name');?>
:</span>
            <div id="orders-info-tName"></div>
            <span><?php echo $_smarty_tpl->getConfigVariable('sum');?>
:</span>
            <div id="orders-info-nPrice"></div>
        </div>
    </div>

    <div id="tmp-sort-root-item-orders">
        <div class="sorting "     field="fView"  style="margin-right: 41px; width: 15px;"></div>
        <div class="sorting DESC" field="dLast"  style="margin-right: 6px; width: 96px;"><?php echo $_smarty_tpl->getConfigVariable('date');?>
</div>
        <div class="sorting "     field="tPhone" style="margin-right: 6px; width: 60px;"><?php echo $_smarty_tpl->getConfigVariable('sum');?>
</div>
        <div class="sorting "     field="tDesc"  style="margin-right: 6px; width: 235px;"><?php echo $_smarty_tpl->getConfigVariable('address');?>
</div>
    </div>


    <div id="tmp-sort-orders-item">
        <li id="g%id%" title="ID: %code%" class="menusItem %hide%">
            <div>
                <span>%desc2%</span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-closethick" title="<?php echo $_smarty_tpl->getConfigVariable('delete');?>
"></span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 ui-icon-pencil" title="<?php echo $_smarty_tpl->getConfigVariable('edit');?>
"></span>
                <span class="<?php echo $_smarty_tpl->tpl_vars['CLS']->value['hide'];?>
 <?php echo $_smarty_tpl->tpl_vars['bClass']->value;?>
 %hideSpan%" title="%hideTitle%"></span>
                <span style="float: right; margin-right: 5px;">%date%</span>
                <span style="float: right; margin-right: 9px;">%price%</span>
            </div>
        </li>
    </div>

    <div id="tmp-dialog-orders">

        <table title="%title%">
            <tr>
                <td colspan="2" class="error">&nbsp;</td>
            </tr>
            <tr>
                <td width="100"><?php echo $_smarty_tpl->getConfigVariable('name');?>
:</td>
                <td>%name%</td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('sum');?>
:</td>
                <td>%price%</td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('phone');?>
:</td>
                <td>%phone%</td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('address');?>
:</td>
                <td><input style="width:350px;" value="%desc%" /></td>
            </tr>
            <tr>
                <td><?php echo $_smarty_tpl->getConfigVariable('status');?>
</td>
                <td>
                    <select>
                        <option value="hide"><?php echo $_smarty_tpl->getConfigVariable('unconfirmed');?>
</option>
                        <option value="show" %ch2%<?php ?>><?php echo $_smarty_tpl->getConfigVariable('confirmed');?>
</option>
                        <option value="do" %ch3%<?php ?>><?php echo $_smarty_tpl->getConfigVariable('done');?>
</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td valign="top"><?php echo $_smarty_tpl->getConfigVariable('description');?>
</td>
                <td>%text%</td>
            </tr>
        </table>

    </div>

</div>
<?php }} ?>
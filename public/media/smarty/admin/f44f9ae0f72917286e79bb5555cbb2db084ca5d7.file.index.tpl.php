<?php /* Smarty version Smarty-3.1.5, created on 2012-05-20 04:03:11
         compiled from "/home/a/afclinic/public_html/tpl/admin/html/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18541952534fb834bfa16b43-52059027%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f44f9ae0f72917286e79bb5555cbb2db084ca5d7' => 
    array (
      0 => '/home/a/afclinic/public_html/tpl/admin/html/index.tpl',
      1 => 1337471956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18541952534fb834bfa16b43-52059027',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'p' => 0,
    'action' => 0,
    'PATH' => 0,
    'wmain' => 0,
    'TMP' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4fb834bfc1bf7',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4fb834bfc1bf7')) {function content_4fb834bfc1bf7($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <title><?php echo $_smarty_tpl->tpl_vars['p']->value['title'];?>
</title>

    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php if ($_smarty_tpl->tpl_vars['action']->value!='login'){?>
    <script src="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
jquery.js" type="text/javascript"></script>
    <script src="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
jquery.elrte.1.3.js" type="text/javascript"></script>
    <script src="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
i18n/elrte.ru.js" type="text/javascript"></script>
    <script src="js.php" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
css/smoothness/jquery.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
css/elrte.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['jslib'];?>
css/elrte-inner.css"/>
    <?php }?>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['root'];?>
css.php"/>
</head>
<body>
<?php if ($_smarty_tpl->tpl_vars['action']->value=='login'){?>


<?php echo $_smarty_tpl->getSubTemplate ("login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"Login"), 0);?>


<?php }else{ ?>
<?php  $_config = new Smarty_Internal_Config("lang.conf", $_smarty_tpl->smarty, $_smarty_tpl);$_config->loadConfigVars("admin_page", 'local'); ?>
<?php $_smarty_tpl->tpl_vars['wmain'] = new Smarty_variable(1100, null, 0);?>
<?php $_smarty_tpl->tpl_vars["aClass"] = new Smarty_variable("item-icon ui-state-default ui-corner-all ui-icon", null, 0);?>
<?php $_smarty_tpl->tpl_vars["bClass"] = new Smarty_variable("item-icon-r ui-state-default ui-corner-all ui-icon", null, 0);?>


<table width="<?php echo $_smarty_tpl->tpl_vars['wmain']->value;?>
" cellpadding="0" cellspacing="0" style="margin: 1px auto; border: 1px solid #cccccc;">
<tr>
<td valign="top">
<!-- LEFT MENU :begin -->
    <div id="lpanel">
        <div id="menu">
            <ul class="ui-widget ui-helper-clearfix">
                <li id="toggle" class="ui-state-default ui-corner-all" name="block" title="<?php echo $_smarty_tpl->getConfigVariable('adm_toogle_tree');?>
">
                    <span class="ui-icon ui-icon-note"></span>
                </li>
                <li id="add" class="ui-state-default ui-corner-all" title="<?php echo $_smarty_tpl->getConfigVariable('adm_add_page');?>
">
                    <span class="ui-icon ui-icon-plusthick"></span>
                </li>
                <li id="up" class="ui-state-default ui-corner-all" title="<?php echo $_smarty_tpl->getConfigVariable('adm_up_sort');?>
">
                    <span class="ui-icon ui-icon-arrowthick-1-n"></span>
                </li>
                <li id="down" class="ui-state-default ui-corner-all" title="<?php echo $_smarty_tpl->getConfigVariable('adm_down_sort');?>
">
                    <span class="ui-icon ui-icon-arrowthick-1-s"></span>
                </li>
                <li id="trash" class="ui-state-default ui-corner-all" title="<?php echo $_smarty_tpl->getConfigVariable('adm_trash');?>
">
                    <span class="ui-icon ui-icon-trash"></span>
                </li>
                <li class="ui-state-default ui-corner-all">
                    <span style="display: block;width: 0px;height:	16px"></span>
                </li>

                <li id="home" class="ui-state-default ui-corner-all" title="На сайт" style="margin-right: 10px;">
                    <a href="/" target="_blank">
                    <span class="ui-icon ui-icon-home"></span>
                    </a>
                </li>

                <li id="cancel" class="ui-state-default ui-corner-all" title="<?php echo $_smarty_tpl->getConfigVariable('adm_logout');?>
">
                    <span class="ui-icon ui-icon-power"></span>
                </li>



                <li id="options" class="ui-state-default ui-corner-all" style="float: right;" title="<?php echo $_smarty_tpl->getConfigVariable('adm_options');?>
">
                    <span class="ui-icon ui-icon-transferthick-e-w"></span>
                </li>
                <li id="loader" class="ui-state-default ui-corner-all" style="float: right;display:none">
                    <span style="line-height:14px;"><img src="<?php echo $_smarty_tpl->tpl_vars['PATH']->value['img'];?>
ajax-loader.gif" style="margin-top:2px;" /></span>
                </li>
            </ul>
        </div>


        <!-- Main tree of item :begin -->
        <ul class="myTree cBox" id="myTree">
            <li><span class="ui-icon ui-icon-power"></span> .. </li>
        </ul>
        <!-- Main tree of item :end -->

        <!-- Trash tree of item :begin -->
        <ul class="myTrash cBox" id="myTrash" style="display: none;">
            <li style="list-style: none;"><span class="ui-icon ui-icon-trash"></span></li>
        </ul>
        <!-- Trash tree of item :end -->
        &nbsp;
    </div>
<!-- LEFT MENU :end -->
</td>
<td valign="top" width="740" height="647">
<div id="main-content">
    <div id="opanel" class="centralpanel">
        <ul class="tpanel">
            <li><a href="#tSets"><?php echo $_smarty_tpl->getConfigVariable('adm_settings');?>
</a></li>
            <li><a href="#tMenus"><?php echo $_smarty_tpl->getConfigVariable('adm_menus');?>
</a></li>
        </ul>
        <div class="content">
            <div id="tSets" class="tab">
                <div class="leftBar"><ul id="tSetsLeft"></ul></div>
                <div class="rightBar" id="tSetsRight"></div>
            </div>
            <div id="tMenus" class="tab">
                <div class="leftBar"><ul id="tMenusLeft"></ul></div>
                <div class="rightBar" id="tMenusRight"></div>
            </div>
        </div>
    </div>

    <div id="cpanel" class="centralpanel">
        <ul class="tpanel">
            <li><a id="firststep" href="#tOption"><?php echo $_smarty_tpl->getConfigVariable('adm_options');?>
</a></li>
            <li><a href="#tCont"><?php echo $_smarty_tpl->getConfigVariable('adm_content');?>
 <select id="tCont-select"><option value="725"></option></select></a></li>
            <li><a href="#tLinks"><?php echo $_smarty_tpl->getConfigVariable('adm_links');?>
</a></li>
        </ul>
        <div class="content">
            <div id="tOption" class="tab">
                <div class="el-rte" style="width: 725px;">
                    <div class="toolbar">
                        <ul class="panel-save first">
                            <li class="save rounded-3" title="<?php echo $_smarty_tpl->getConfigVariable('save');?>
"></li>
                            <li id="items-clean-field" class="removeformat rounded-3" title="<?php echo $_smarty_tpl->getConfigVariable('clear_fields');?>
"></li>
                        </ul>
                    </div>
                </div>

                <div class="optionTitle"><?php echo $_smarty_tpl->getConfigVariable('name');?>
:</div>
                <div class="optionValue"><input class="icont" id="tName" name="tName" /></div>
                <div class="optionTitle"><?php echo $_smarty_tpl->getConfigVariable('alias');?>
:</div>
                <div class="optionValue"><input class="icont" id="tAlias" name="tAlias" /></div>
                <div class="optionTitle"><?php echo $_smarty_tpl->getConfigVariable('link');?>
:</div>
                <div class="optionValue"><input class="icont" id="tLink" name="tLink" /></div>
                <div class="optionTitle"><?php echo $_smarty_tpl->getConfigVariable('description');?>
:</div>
                <div class="optionValue"><input class="icont" id="tDesc" name="tDesc" /></div>
                <div class="optionTitle">&lt;TITLE&gt;</div>
                <div class="optionValue"><input class="icont" id="tTitle" name="tTitle" /></div>
                <div class="optionTitle">&lt;META description&gt;</div>
                <div class="optionValue"><input class="icont" id="tMetaDesc" name="tMetaDesc" /></div>
                <div class="optionTitle">&lt;META keywords&gt;</div>
                <div class="optionValue"><input class="icont" id="tMetaKey" name="tMetaKey" /></div>
                <div class="optionTitle"><?php echo $_smarty_tpl->getConfigVariable('template');?>
</div>
                <div class="optionValue"><select class="icont" id="tTemplate" name="tTemplate" style="width:200px;"></select></div>
            </div>
            <div id="tCont" class="tab">
                <textarea id="tText" name="tText" style="width:720px;height:600px;"></textarea>
            </div>

            <div id="tLinks" class="tab">
                <div style="float: left; width: 250px;">
                    <div><?php echo $_smarty_tpl->getConfigVariable('adm_selected_links');?>
</div>
                    <div style="overflow-y: auto; height:580px;"><ul id="myLinks"></ul></div>
                </div>
                <div style="float: left; width: 250px;">
                    <div><?php echo $_smarty_tpl->getConfigVariable('adm_link_list');?>
</div>
                    <div style="overflow-y: auto; height:580px;"><ul id="links"></ul></div>
                </div>
            </div>
        </div>
    </div>

</div>
</td>
</tr>
</table>

    <?php echo $_smarty_tpl->getSubTemplate ("templates.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <?php  $_smarty_tpl->tpl_vars['class'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['class']->_loop = false;
 $_smarty_tpl->tpl_vars['nm'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['TMP']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['class']->key => $_smarty_tpl->tpl_vars['class']->value){
$_smarty_tpl->tpl_vars['class']->_loop = true;
 $_smarty_tpl->tpl_vars['nm']->value = $_smarty_tpl->tpl_vars['class']->key;
?>
        <?php echo $_smarty_tpl->getSubTemplate ("tmp.".($_smarty_tpl->tpl_vars['nm']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    <?php } ?>

<?php }?>

</body>
</html>
<?php }} ?>
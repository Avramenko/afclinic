/*
JXUploader v0.2.1
Release Date: November 6, 2010
r0.2.0 2009-12-15
Copyright (c) 2009 Arthur Creek
*/

jQuery.extend(
{
    getSWF: function(n)
    {
        //return document.getElementById(n);
        if(navigator.appName.indexOf("Microsoft") != -1)
        {
            return window[n];
        }
        else
        {
            return document[n];
        }
    },
    serialize: function(o)
    {
        var s = '';
        $.each(o,function(n,v){s += '&' + n + '=' + v;});
        return s.substr(1);
    }

});


if(jQuery)(function($){
$.extend($.fn,
{
    JXUploader:function(options)
    {
        var obj = $(this), id = obj.attr('id');
        sets = $.extend(
        {
            type            : 'multi',
            swf             : null,             // The path to the JXUploader swf file
            expressInstall  : null,             // The path to the express install swf file
            wmode           : 'opaque',         // The wmode of the flash file
            scriptAccess    : 'sameDomain',     // Set to "always" to allow script access across domains

            script          : 'jxuploader.php', // The path to the JXUploader backend script
            scriptData      : {},               // The POST data in the backend script
            scriptFile      : 'Filedata',       // The name of the file object in the backend script

            auto            : false,            // Autostart upload flag
            fileExt         : '',               // Eneble file types list
            fileDesc        : '',               // Eneble file types descriptions list
            limQueue        : 10,                // The maximum size of the file queue
            limThread       : 1,                // The number of thread uploads allowed
            limSize         : false,            // File size limit, '0' or 'false' - unlimited
            displayData     : 'percent',        // Displaying "none", "percent", "speed", "all"

            height          : 30,               // The height of the flash button
            width           : 110,              // The width of the flash button
            bgColor         : 'D0D0D0',         // Background color of button
            color           : '000000',         // Text color of button
            size            : 12,               // Text size of button
            bold            : false,            // Text weight of button
            italic          : false,            // Text style of button
            underline       : false,            // Underline or not
            font            : 'Arial',          // Font family
            text            : '',               // Text of Browse button
            image           : '',               // Image of Browse button
            text2           : '',               // Text of Upload button
            image2          : '',               // Image of Upload button

            alertExtError   : 'Wrong extention',
            alertLimThread  : 'The queue is full.  The max size is ',

            onInit          : function() {},    // Function to run when JXUploader is initialized
            onSelect        : function() {},    // Function to run when a file is selected
            onQueueFull     : function() {},    // Function to run when the queue reaches capacity
            onCancel        : function() {},    // Function to run when an item is cleared from the queue
            onError         : function() {},    // Function to run when an upload item returns an error
            onProgress      : function() {},    // Function to run each time the upload progress is updated
            onComplete      : function() {},    // Function to run when an upload is completed
            onAllComplete   : function() {}     // Function to run when all uploads are completed
        },
        options);
        sets.image2 = sets.image2 ? sets.image2 : (sets.image ? sets.image : '');
        sets.text = sets.text ? sets.text : (sets.image ? '' : 'Browse');
        sets.text2 = sets.text2 ? sets.text2 : (sets.image2 ? '' : 'Upload');
        if($.browser.msie) sets.auto = true;
        if($.browser.safari || $.browser.msie) sets.type = 'multi';
        sets.extList = [];
        $.each(sets.fileExt.split(/;|\|/).sort(),function(i,v){
            if($.inArray(v,sets.extList) == -1) sets.extList[sets.extList.length] = v;
        });

        obj.before('<div id="' + id + 'Queue" class="jxuploaderQueue">');
        var aDiv = $('<div>')
        .css({
            width: sets.width,
            height: sets.height,
            position: 'relative',
            overflow: 'hidden',
            float: 'left',
            margin:5
        }),
        bDiv = $('<div>')
        .css({
            width: sets.width,
            height: sets.height,
            //"line-height": sets.height,
            paddingTop: Math.floor((sets.height-sets.size)/2-3),
            backgroundColor: '#'+sets.bgColor,
            backgroundRepeat: 'no-repeat',
            color: sets.color,
            fontSize: sets.size,
            fontFamily: sets.font,
            fontWeight: sets.bold?'bold':'normal',
            fontStyle: sets.italic?'italic':'normal',
            textAlign: 'center',
            textDecoration: sets.underline ? 'underline' : 'none',
            position: 'absolute',
            left:0,
            cursor:'pointer'
        });

        if(sets.type == 'multi')
        {
            var data = {};
            data.uploadID = id;

            data.script     = sets.script;
            data.scriptData = escape($.serialize(sets.scriptData));
            data.scriptFile = sets.scriptFile;

            data.auto       = sets.auto ? 1 : '';
            data.fileExt    = sets.fileExt;
            data.fileDesc   = sets.fileDesc;
            data.limQueue   = sets.limQueue;
            data.limThread  = sets.limThread;
            data.limSize    = sets.limSize ? sets.limSize : '';

            data.width      = sets.width;
            data.height     = sets.height;
            data.bgColor    = sets.bgColor;
            data.color      = sets.color;
            data.size       = sets.size;
            data.bold       = sets.bold ? true : '';
            data.italic     = sets.italic ? true : '';
            data.underline  = sets.underline ? true : '';
            data.font       = escape(sets.font);
            data.text       = escape(sets.text);
            data.image      = escape(sets.image);


            aDiv.clone().attr({id: id + 'Uploader'}).appendTo(obj)
            .flash({
                id: id + 'SWFObject',
                name: id + 'SWFObject',
                swf: sets.swf,
                width: sets.width,
                height: sets.height,
                expressInstall: sets.expressInstall,
                flashvars: data,
                params:
                {
                    'quality':'high',
                    'wmode':sets.wmode,
                    'allowScriptAccess':sets.scriptAccess
                }
            });

            if(!sets.auto)
            {
                aDiv.clone().attr({id: id + 'Upload'}).appendTo(obj)
                .append(
                    bDiv.clone()
                    .css({backgroundImage: 'url(' + sets.image2 + ')'})
                    .html(sets.text2)
                    .click(function(){obj.trigger('start');})
                )
            }

            if(typeof(sets.onInit) == 'function')
            {
                sets.onInit(obj);
            }

            if(typeof(sets.onOpen) == 'function')
            {
                obj.bind("uploadOpen", sets.onOpen);
            }
            if(typeof(sets.onSelectOnce) == 'function')
            {
                obj.bind("uploadSelectOnce", sets.onSelectOnce);
            }
            if(typeof(sets.onClearQueue) == 'function')
            {
                obj.bind("uploadClearQueue", sets.onClearQueue);
            }
            if(typeof(sets.onAllComplete) == 'function')
            {
                obj.bind("uploadAllComplete",
                    function(event, uploadObj)
                    {
                        if(sets.onAllComplete(uploadObj) !== false) errorArray = [];
                    }
                );
            }

            var errorArray = [];

            obj.bind("uploadSelect",function(event, oFile)
            {
                if(sets.onSelect(oFile) !== false)
                {
                    var byteSize = Math.round(oFile.size / 1024 * 100) * .01;
                    var suffix = 'KB';
                    if(byteSize > 1000)
                    {
                        byteSize = Math.round(byteSize *.001 * 100) * .01;
                        suffix = 'MB';
                    }
                    var sizeParts = byteSize.toString().split('.');
                    byteSize = sizeParts[0] + (sizeParts.length > 1 ? '.' + sizeParts[1].substr(0,2) : '');
                    fileName = oFile.name.length > 20 ? oFile.name.substr(0,20) + '...' : oFile.name;

                    $('<div id="' + id + oFile.id + '" class="jxuploaderQueueItem">' +
                            '<span class="fileName">' + fileName + ' (' + byteSize + suffix + ')</span>' +
                            '<span class="jxuploaderDisplay"/>' +
                            '<div class="jxuploaderProgress">'+
                                '<div id="' + id + oFile.id + 'ProgressBar" class="jxuploaderProgressBar"/>' +
                            '</div>' +
                        '</div>'
                    )
                    .appendTo('#' + id + 'Queue')
                    .each(function(){
                        if(!$.browser.msie)
                            $(this).prepend(
                                $('<div class="jxuploaderCancel ui-icon ui-icon-closethick"/>')
                                .click(function(){obj.trigger('cancel',[oFile.id])})
                            )
                    });
                }
            })
            .bind("uploadQueueFull",function(event, limQueue)
            {
                if(sets.onQueueFull(limQueue) !== false)
                {
                    alert(sets.alertLimThread + ' '  + limQueue + '.');
                }
            })
            .bind("uploadCancel",function(event, oFile)
            {
                if(sets.onCancel(oFile) !== false)
                {
                    $("#" + id + oFile.id).fadeOut(500, function(){$(this).remove()});
                }
            })
            .bind("uploadError",function(event, oFile, oData)
            {
                if(sets.onError(oFile, oData) !== false)
                {
                    var fileArray = new Array(oFile, oData);
                    errorArray.push(fileArray);
                    alert(oData.type);
                    $(".jxuploaderDisplay",$("#"+id+oFile.id).addClass('jxuploaderError')).text(" - "+oData.type+" Error");
                }
            })
            .bind("uploadProgress",function(event, oFile, oData)
            {
                if(sets.onProgress(oFile, oData) !== false)
                {
                    $("#" + id + oFile.id + "ProgressBar").css({width:oData.percent + '%'});
                    var displayData = ' ';
                    switch(sets.displayData)
                    {
                        case 'percent': displayData = ' - ' + oData.percent + '%'; break;
                        case 'speed': displayData = ' - ' + oData.speed + 'KB/s'; break;
                        case 'all': displayData = ' [' + oData.percent + '%] - ' + oData.speed + 'KB/s'; break;
                    }
                    $("#" + id + oFile.id + " .jxuploaderDisplay").text(displayData);
                }
            })
            .bind("uploadComplete",function(event, oFile, oResp)
            {
                oResp = eval('('+unescape(oResp)+')');
                if(sets.onComplete(oFile,oResp) !== false)
                {
                    $("#" + id + oFile.id)
                    .each(function(){$(".jxuploaderDisplay",this).text(' - Completed')})
                    .fadeOut(500,function(){$(this).remove()});
                }
            })
            .bind('start',function(e,fid)
            {
                $.getSWF(id + 'SWFObject').startUpload(fid);
            })
            .bind('cancel',function(e,fid)
            {
                $.getSWF(id + 'SWFObject').cancelFile(fid);
            })
            .bind('clear',function(e)
            {
                $.getSWF(id + 'SWFObject').clearQueue();
            })
        }
    }
})})(jQuery);

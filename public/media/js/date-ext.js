Number.prototype.pad = function (n,p){
        var s = '' + this;
        p = p || '0';
        n = n || 2;
        while (s.length < n) s = p + s;
        return s;
};
Date.M = ['January','February','March','April','May','June','July','August','September','October','November','December'];
Date.m = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
Date.D = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
Date.d = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
Date.sf = '%B %d %Y, %T';
Date.sd = '%m/%d/%Y';
Date.st = '%H:%M:%S';
Date.o = false;
Date.dpm = [31,28,31,30,31,30,31,31,30,31,30,31];
Date.startYear = 1970;

Date.q = {
    ' ' :'\\s',
    ':' :'\\:',
    '-' :'\\-',
    '.' :'\\.',
    ',' :'\\,',
    '/' :'\\/',
    '%d':'(0?[1-9]|[1-2][0-9]|3[01])',
    '%H':'([01]?\\d|2[0-3])',
    '%I':'(0?\\d|1[0-2])',
    '%M':'([0-5]?\\d)',
    '%m':'(0?\\d|1[0-2])',
    '%p':'\\s*([ap]\\.?m\\.?)?',
    '%S':'([0-5]?\\d|6[01])',
    '%Y':'(\\d{4})',
    '%y':'(\\d{2})',
    '%' :'\\%'
};
Date.p = ["%Y-%m-%d %H:%M:%S%p",    "%m/%d/%Y %H:%M:%S%p",  "%m.%d.%Y %H:%M:%S%p",  "%m/%d/%y %H:%M:%S%p",  "%m.%d.%y %H:%M:%S%p",
          "%Y-%m-%d %H:%M%p",       "%m/%d/%Y %H:%M%p",     "%m.%d.%Y %H:%M%p",     "%m/%d/%y %H:%M%p",     "%m.%d.%y %H:%M%p",
          "%Y-%m-%d",               "%m/%d/%Y",             "%m.%d.%Y",             "%m/%d/%y",             "%m.%d.%y"];
Date.r = ["%","Yy","m","d","HI","M","S","s","p"];


Date.prototype.f = {
    A: function(d)
    {
        return Date.D[d.getDay()];
    },
    a: function(d)
    {
        return Date.d[d.getDay()];
    },
    B: function(d)
    {
        return Date.M[d.getMonth()];
    },
    b: function(d)
    {
        return Date.m[d.getMonth()];
    },
    C: function(d)
    {
        return Math.floor(d.getFullYear()/100);
    },
    c: function(d)
    {
        return d.toString();
    },
    D: function(d)
    {
        return d.f.m(d) + '/' + d.f.d(d) + '/' + d.f.y(d);
    },
    d: function(d)
    {
        return d.getDate().pad(2);
    },
    e: function(d)
    {
        return d.getDate().pad(2,' ');
    },
    H: function(d)
    {
        return d.getHours().pad(2);
    },
    h: function(d)
    {
        return d.f.b(d);
    },
    I: function(d)
    {
        return( d.getHours() % 12 || 12).pad(2);
    },
    j: function(d,p)
    {
        var t = d.getDate();
        var m = d.getMonth() - 1;
        if(m > 1 && d.isLeapYear())
        {
            ++t;
        }
        while(m > -1)
        {
            t += Date.dpm[m--];
        }
        if(p) return t;
        return t.pad(3);
    },
    M: function(d)
    {
        return d.getMinutes().pad(2);
    },
    m: function(d)
    {
        return(d.getMonth() + 1).pad(2);
    },
    n: function(d)
    {
        return "\n";
    },
    p: function(d)
    {
        return(d.getHours()>11) ? 'PM' : 'AM';
    },
    R: function(d)
    {
        return d.f.H(d) + ':'+d.f.M(d);
    },
    r: function(d)
    {
        return d.f.I(d) + ':'+d.f.M(d) + ':'+d.f.S(d) + ' ' + d.f.p(d);
    },
    S: function(d)
    {
        return d.getSeconds().pad(2);
    },
    T: function(d)
    {
        return d.f.H(d) + ':' + d.f.M(d) + ': ' + d.f.S(d);
    },
    t: function(d){
        return "\t";
    },
    U: function(d){
        var w = new Date(d.f.Y(d),0,1).getDay();
        return Math.ceil((d.f.j(d,1) + (w!=0 ? -7+w : 0))/7).pad(2);
    },
    u: function(d)
    {
        return(d.getDay()||7);
    },
    V: function(d)
    {
        var a = 864e5, b = 7 * a;
        var DC3 = Date.UTC(d.f.Y(d), d.getMonth(), d.getDate() + 3) / a, AWN = Math.floor(DC3 / 7),
        wy = new Date(AWN * b).getUTCFullYear();
        return (AWN-Math.floor(Date.UTC(wy,0,7)/b)+1).pad();
    },
    v: function(d){
        return d.f.e(d)+'-'+d.f.b(d)+'-'+d.f.Y(d);
    },
    W: function(d){
        var w = new Date(d.f.Y(d),0,1).getDay();
        return Math.ceil((d.f.j(d,1) + (w!=1 ? -8+w : 0))/7).pad(2);
    },
    w: function(d){
        return d.getDay();
    },
    X: function(d)
    {
        return d.F(Date.st);

    },
    x: function(d)
    {
        return d.F(Date.sd);
    },
    Y: function(d)
    {
        return d.getFullYear();
    },
    y: function(d)
    {
        return(d.getYear()%100).pad(2);
    },
    Z: function(d)
    {
        return 'GMT';
    },
    z: function(d)
    {
        var t = d.getTimezoneOffset();
        return (t > 0 ? '-' : '+') + (Math.abs(t)/60).pad(2) + (Math.abs(t) % 60).pad(2);
    },
    '%': function(d){
        return '%';
    }
};

Date.prototype.F = function(f)
{
    if(typeof f != 'string')
    {
        f = Date.sf;
    }
    switch(f)
    {
        case 'date':
            f = '%Y-%m-%d';
        break;
        case 'db':
            f = '%Y-%m-%d %H:%M:%S';
        break;
        case 'time':
            f = '%H:%M:%S';
        break;
        case 'compact':
            f = '%Y%m%dT%H%M%S';
        break;
        case 'iso8601':
            f = '%Y-%m-%dT%H:%M:%S%z';
        break;
        case 'rfc822':
            f = '%a, %d %b %Y %H:%M:%S %Z';
        break;
        case 'short':
            f = '%d %b %H:%M';
        break;
        case 'long':
            f = '%B %d, %Y %H:%M';
        break;
    }
    var r = '',n = 0;
    while(n < f.length)
    {
        var c = f.substring(n,n+1);
        if(c == '%')
        {
            c = f.substring(++n,n+1);
            r += (this.f[c]) ? this.f[c](this) : c;
        }
        else
        {
            r += c;
        }
        ++n;
    }
    return r;
};

Date.prototype['strftime'] = Date.prototype.F;

Date.prototype['format'] = Date.prototype.F;

Date.prototype.parse = function(s)
{
    var d = Date.parse(s);
    if(d)this.setTime(d.getTime());
    return this;
},

Date.prototype.arithmetic = function(t,n,d)
{
    if(d == 'dec') n = -1*n;
    switch(t)
    {
        case 'year':
            this.setYear(this.getYear()+n)
        break;
        case 'month':
            this.setMonth(this.getMonth()+n)
        break;
        case 'date':
            this.setDate(this.getDate()+n)
        break;
        case 'hours':
            this.setHours(this.getHours()+n)
        break;
        case 'min':
            this.setMinutes(this.getMinutes()+n)
        break;
        case 'sec':
            this.setSeconds(this.getSeconds()+n)
        break;
        case 'ms':
            this.setDate(this.getDate()+n)
        break;
    }
    return this;
},

Date.prototype.inc = function(t,n)
{
    if(n == undefined) n = 1;
    this.arithmetic(t,n,'inc');
    return this;
}

Date.prototype.dec = function(t,n)
{
    if(n == undefined) n = 1;
    this.arithmetic(t,n,'dec');
    return this;
}


Date.prototype.get = function(f)
{
    switch(f)
    {
        case 'dayofyear':
            return parseInt(parseFloat(this.F("%j")));
        case 'lastdayofmonth':
            var m = this.getMonth();
            if(this.isLeapYear() && m == 1)
            {
                return 29;
            }
            return Date.dpm[m];
        case 'gmtoffset':
            return this.F('%z');
        case 'timestamp':
            return Math.floor(this.getTime()/1000);
    }
    return '';
};

Date.prototype.isLeapYear = function()
{
    var y = this.getYear();
    if((y%4) == 0)
    {
        if((y%100 == 0)&&(y%400) != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    return false;
};

Date.prototype.clone = function()
{
    return new Date(this.getTime());
};

Date.prototype.clearTime = function()
{
    this.setHours(0);
    this.setMinutes(0);
    this.setSeconds(0);
    this.setMilliseconds(0)
};

Date.addParser = function(s)
{
    Date.p[Date.p.length] = s;
};

Date.parse = function(s)
{
    var d,i,j,k,p,q;
    var a = Array();
    for(i = 0; i< Date.p.length;i++)
    {
        p = Date.p[i];
        for(j in Date.q)
        {
            p = p.replace(RegExp("\\"+j,"g"),Date.q[j]);
        }
        d = s.match(RegExp('\^'+p+'\$'),"g");
        if(d)
        {
            break;
        }
    }
    if(d)
    {
        q = Date.p[i].replace(RegExp("[^"+Date.r.join("")+"]","g"),'').split("%");
        for(i = 0;i<q.length;i++)
        {
            for(j = 0; j < Date.r.length;j++)
            {
                if(Date.r[j].indexOf(q[i])>=0)
                {
                    q[i] = j;
                    break;
                }
            }
        }
        for(i = 0; i < q.length;i++)
        {
            switch(q[i])
            {
                case 1:
                    if(d[i].length == 4)
                    {
                        a[1] = d[i];
                    }
                    else
                    {
                        var c = parseInt(d[i][0])*10+parseInt(parseFloat(d[i][1]));
                        var e = Date.startYear%100;
                        a[1] = Date.startYear - e + c + (c<e?100:0);
                    }
                break;
                case 2:case 3:case 4:case 5:case 6:case 7:
                    a[q[i]] = d[i]?parseInt(parseFloat(d[i])):0;
                break;
                case 8:
                    a[8] = d[i]?d[i].substring(0,1).toLowerCase():0;
                break;
            }
        }
        return new Date(a[1]?a[1] : new Date().getFullYear(),
                        a[2]?a[2]-1:0,
                        a[3]?a[3]:1,
                        a[4]?(a[4]>12||a[4]==0?a[4]:(a[4]==12?(a[8]=='a'?0:12):(a[8]=='p'?a[4]+12:a[4]))):0,
                        a[5]?a[5]:0,
                        a[6]?a[6]:0,
                        a[7]?a[7]:0);
    }
    return false;
};

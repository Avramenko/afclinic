<?php
$this->pageTitle = Yii::t('PublicationModule.publication', 'Publication');
$this->breadcrumbs = array(Yii::t('PublicationModule.publication', 'Publication'));
?>

<h1>Публикации</h1>

<?php $this->widget(
    'zii.widgets.CListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView'     => '_view',
    )
); ?>

<div class="row">
    <div class="col-sm-12">
        <h4><strong><?php echo CHtml::link(
                    CHtml::encode($data->title),
                    array('/publication/publication/show/', 'alias' => $data->alias)
                ); ?></strong></h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <p> <?php echo $data->short_text; ?></p>

        <p><?php echo CHtml::link(
                Yii::t('PublicationModule.publication', 'read...'),
                array('/publication/publication/show/', 'alias' => $data->alias),
                array('class' => 'btn')
            ); ?></p>
    </div>
</div>

<hr>

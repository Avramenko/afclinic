var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,				
				lat: 59.9282075,
				lng: 30.3185844
			  });
			  
			  var marker = map.addMarker({
				lat: 59.9282075,
                lng: 30.3185844,
	            title: 'Advanced Fertility Clinic'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat : 59.9282075,
		        lng : 30.3185844
		      });
		    });
		}        

    };
}();
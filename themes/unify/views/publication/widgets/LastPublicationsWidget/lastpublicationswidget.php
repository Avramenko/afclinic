<?php Yii::import('application.modules.publication.PublicationModule'); ?>
<div class='portlet'>
    <div class='portlet-decoration'>
        <div class='portlet-title'><?php echo Yii::t('PublicationModule.publication', 'Publication'); ?></div>
    </div>
    <div class='portlet-content'>
        <?php if (isset($models) && $models != array()): ?>
            <ul>
                <?php foreach ($models as $model): ?>
                    <li><?php echo CHtml::link(
                            $model->title,
                            array('/publication/publication/show/', 'alias' => $model->alias)
                        ); ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>

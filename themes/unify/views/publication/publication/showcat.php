<?php
/**
 * Отображение для ./themes/default/views/publication/publication/publication.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
?>
<?php $this->pageTitle = $cat->name; ?>

<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publication/index/'),
    CHtml::encode($cat->name)
);
?>

<div class="post">
    <div class="row">
        <div class="col-sm-12">
            <h4><strong><?php echo CHtml::encode($cat->name); ?></strong></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if ($cat->image): ?>
                <?php echo CHtml::image($cat->getImageUrl(), $cat->name); ?>
            <?php endif; ?>
<?php $this->widget(
    'zii.widgets.CListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView'     => '_view',
    )
); ?>
        </div>
    </div>
</div>

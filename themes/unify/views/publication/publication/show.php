<?php
/**
 * Отображение для ./themes/default/views/publication/publication/publication.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
?>
<?php $this->pageTitle = $publication->title; ?>

<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publication/index/'),
    CHtml::encode($publication->title)
);
?>

<div class="post">
    <div class="row">
        <div class="col-sm-12">
            <h4><strong><?php echo CHtml::encode($publication->title); ?></strong></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if ($publication->image): ?>
                <?php echo CHtml::image($publication->getImageUrl(), $publication->title); ?>
            <?php endif; ?>
            <p> <?php echo $publication->full_text; ?></p>

        </div>
    </div>
</div>

<?php
Yii::import('application.modules.menu.components.YMenu');
$this->widget(
    'booster.widgets.TbMenu',
    [
        'type' => 'navbar',
        'items' => $this->params['items']
    ]
);
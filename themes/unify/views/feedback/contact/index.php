<?php
$this->layout = '//layouts/column2';
$this->title = [Yii::t('FeedbackModule.feedback', 'Contacts'), Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [Yii::t('FeedbackModule.feedback', 'Contacts')];
Yii::import('application.modules.feedback.FeedbackModule');
Yii::import('application.modules.install.InstallModule');
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerScriptFile('http://maps.google.com/maps/api/js?sensor=true');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/gmap/gmap.js');
Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/pages/page_contacts.js');
?>

<div class="headline"><h1 class="heading-sm"><?= Yii::t('FeedbackModule.feedback', 'Contacts'); ?></h1></div>

<address class="md-margin-bottom-40">
    <i class="fa fa-home"></i>190031, Россия, Санкт-Петербург, Спасский пер. 11/1<br>
    Станции метро Сенная площадь, Спасская, Садовая <br>
    <i class="fa fa-phone"></i><a href="tel:+7 (812) 310 00 33">+7 (812) 310 00 33</a> <br>
    <i class="fa fa-phone"></i><a href="tel:+7 (812) 310 41 56">+7 (812) 310 41 56</a> <br>
    <i class="fa fa-globe"></i>Сайт: <a href="/">www.afclinic.ru</a> <br>
    <i class="fa fa-envelope"></i>Email: <a href="mailto:info@afclinic.ru">info@afclinic.ru</a>
</address>

<div class="headline"><h2 class="heading-sm">График работы центра</h2></div>
<i class="fa fa-clock-o"></i> <b>Понедельник- пятница: с 9:00 до 20:00</b> <br>
<i class="fa fa-clock-o"></i> <b>Суббота: с 10:00 до 18:00</b><br>
<i class="fa fa-clock-o"></i> <b>Воскресенье: с 10:00 до 16:00</b><br>

<hr/>

<!-- Basic Map -->
<div class="headline"><h3 class="heading-sm">Карта</h3></div>
<div id="map" class="map margin-bottom-50"></div>
<!-- End Basic Map -->

<!-- Panorama Map -->
<div class="headline"><h3>Панорама</h3></div>
<div id="panorama" class="map margin-bottom-40"></div>
<!-- End Panorama Map -->


<div class="tag-box tag-box-v3">
<div class="headline"><h2 class="heading-sm">Напишите нам!</h2></div>
<?php $this->widget('yupe\widgets\YFlashMessages'); ?>


    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'id'          => 'feedback-form',
            'type'        => 'vertical',
            'htmlOptions' => [
                'class' => 'margin-bottom-40',
            ]
        ]
    ); ?>

    <p class="alert alert-info">
        <?= Yii::t('FeedbackModule.feedback', 'Fields with'); ?> <span
            class="required">*</span> <?= Yii::t('FeedbackModule.feedback', 'are required.'); ?>
    </p>

    <?= $form->errorSummary($model); ?>

    <?php if ($model->type): ?>
        <div class='row'>
            <div class="col-sm-6">
                <?= $form->dropDownListGroup(
                    $model,
                    'type',
                    [
                        'widgetOptions' => [
                            'data' => $module->getTypes(),
                        ],
                    ]
                ); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class='row'>
        <div class="col-sm-6">
            <?= $form->textFieldGroup($model, 'name'); ?>
        </div>
    </div>

    <div class='row'>
        <div class="col-sm-6">
            <?= $form->textFieldGroup($model, 'email'); ?>
        </div>
    </div>

    <div class='row'>
        <div class="col-sm-6">
            <?= $form->textFieldGroup($model, 'theme'); ?>
        </div>
    </div>

    <div class='row'>
        <div class="col-sm-7">
            <?= $form->textAreaGroup(
                $model,
                'text',
                ['widgetOptions' => ['htmlOptions' => ['rows' => 10]]]
            ); ?>
        </div>
    </div>

    <?php if ($module->showCaptcha && !Yii::app()->getUser()->isAuthenticated()): ?>
        <?php if (CCaptcha::checkRequirements()): ?>
            <?php $this->widget(
                'CCaptcha',
                [
                    'showRefreshButton' => true,
                    'imageOptions'      => [
                        'width' => '150',
                    ],
                    'buttonOptions'     => [
                        'class' => 'btn btn-info',
                    ],
                    'buttonLabel'       => '<i class="glyphicon glyphicon-repeat"></i>',
                ]
            ); ?>
            <div class='row'>
                <div class="col-sm-6">
                    <?= $form->textFieldGroup(
                        $model,
                        'verifyCode',
                        [
                            'widgetOptions' => [
                                'htmlOptions' => [
                                    'placeholder' => Yii::t(
                                        'FeedbackModule.feedback',
                                        'Insert symbols you see on image'
                                    )
                                ],
                            ],
                        ]
                    ); ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton',
        [
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('FeedbackModule.feedback', 'Send message'),
        ]
    ); ?>

    <?php $this->endWidget(); ?>

</div>
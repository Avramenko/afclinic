<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 10.04.2016
 * Time: 3:23
 */
$this->breadcrumbs = ['Специалисты'];

$this->widget(
    'bootstrap.widgets.TbListView',
    [
        'dataProvider'       => $model->search(),
        'template'           => '{items}',
        'sorterCssClass'     => 'sorter pull-left',
        'itemView'           => '_hired_item',
        'ajaxUpdate'         => false,
    ]
);
?>
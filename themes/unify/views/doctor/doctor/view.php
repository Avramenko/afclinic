<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('doctor')->getCategory() => array(),
    Yii::t('doctor', 'Специалисты') => array('/doctor/doctor/index'),
    $model->getFullName() . ' / ' . $model->position,
);

$this->title = [$model->title, Yii::app()->getModule('yupe')->siteName];

?>



<div class="row team-v7 no-gutter equal-height-columns">
    <div class="col-md-6 team-v7-img">
        <img src="<?php echo $model->getImageUrl(585, 300)?>" class="img-responsive">
    </div>
    <div class="col-md-6 team-arrow-left">
        <div class="dp-table">
            <div class="equal-height-column dp-table-cell team-v7-in" style="height: 333px;">
                <span class="team-v7-name"><strong><?php echo $model->getFullName();?></strong></span> / <span class="team-v7-position"><?php echo $model->position;?></span>
                <p><?php echo $model->description;?>
                </p>
                <?php $this->widget('application.modules.blog.widgets.ShareWidget'); ?>
                <!--<ul class="list-inline social-icons-v1">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>-->
            </div>
        </div>
    </div>

</div>

<div class="comments-section">
    <?php $this->widget('application.modules.comment.widgets.CommentsWidget', [
        'redirectTo' => Yii::app()->createUrl('/doctor/'.$model->id),
        'model' => $model,
    ]); ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#doctor img').addClass('img-responsive');
        $('pre').each(function (i, block) {
            hljs.highlightBlock(block);
        });
    });
</script>
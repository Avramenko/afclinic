<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 10.04.2016
 * Time: 3:16
 */

?>
<div class="row team-v7 no-gutter equal-height-columns">
    <?php if($index%2 < 1){?>
        <div class="col-md-6 team-v7-img">
            <a href="<?php echo $data->getLink();?>">
                <img src="<?php echo $data->getImageUrl(585, 300)?>" class="img-responsive" alt="<?php echo $data->imageAlt;?>">
            </a>
        </div>
        <div class="col-md-6 team-arrow-left">
            <div class="dp-table">
                <div class="equal-height-column dp-table-cell team-v7-in" style="height: 333px;">
                    <span class="team-v7-name">
                        <strong><?php echo $data->getFullName();?></strong>
                    </span>
                    <span class="team-v7-position"><?php echo $data->position;?></span>
                    <p><?php echo $data->description;?>
                    </p>

                    <!--<ul class="list-inline social-icons-v1">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>-->
                    <a class="btn btn-default" href="<?php echo $data->getLink();?>">Оставить отзыв</a>
                </div>
            </div>
        </div>
    <?php }else{ ?>

        <div class="col-md-6 team-arrow-right">
            <div class="dp-table">
                <div class="equal-height-column dp-table-cell team-v7-in" style="height: 333px;">
                    <span class="team-v7-name">
                        <strong><?php echo $data->getFullName();?></strong>
                    </span>
                    <span class="team-v7-position"><?php echo $data->position;?></span>
                    <p><?php echo $data->description;?>
                    </p>

                    <a class="btn btn-default" href="<?php echo $data->getLink();?>">Оставить отзыв</a>
                    <!--<ul class="list-inline social-icons-v1">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>-->
                </div>
            </div>
        </div>
        <div class="col-md-6 team-v7-img">
            <a href="<?php echo $data->getLink();?>">
                <img src="<?php echo $data->getImageUrl(585, 300)?>" class="img-responsive" alt="<?php echo $data->imageAlt;?>">
            </a>
        </div>
    <?php }?>


</div>

<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
?>
<!--=== Footer v2 ===-->
<div id="footer-v2" class="footer-v2">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-3 md-margin-bottom-40">
                    <a href="/"><img id="logo-footer" class="footer-logo" src="<?=$mainAssets?>/img/logo.png" alt=""></a>
                    <h4>Advanced Fertility Clinic</h4>
                    <p class="margin-bottom-20">Сегодня стать родителями стало проще, доступней и ещё комфортней!</p>

                    <!--<form class="footer-subsribe">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Поиск">
                                <span class="input-group-btn">
                                    <button class="btn-u" type="button">Найти</button>
                                </span>

                        </div>
                    </form>-->
                    <?php $this->widget('application.modules.zendsearch.widgets.SearchBlockWidget');?>
                </div>
                <!-- End About -->

                <!-- Link List -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2 class="heading-sm">Полезные ссылки</h2></div>
                    <?php $this->widget(
                        "application.modules.contentblock.widgets.ContentBlockWidget",
                        array("code" => "menyu-v-podvale"));
                    ?>
                </div>
                <!-- End Link List -->

                <!-- Address -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2 class="heading-sm">Адреса и телефоны</h2></div>
                    <address class="md-margin-bottom-40">
                        <i class="fa fa-home"></i>190031, Россия, Санкт-Петербург, Спасский пер. 11/1<br />
                        Станции метро Сенная площадь, Спасская, Садовая <br/>
                        <i class="fa fa-phone"></i><a href="tel:+7 (812) 310 00 33" class="ya-phone">+7 (812) 310 00 33</a> <br />
                        <i class="fa fa-phone"></i><a href="tel:+7 (812) 310 41 56">+7 (812) 310 41 56</a> <br />
                        <i class="fa fa-globe"></i>Сайт: <a href="/">www.afclinic.ru</a> <br />
                        <i class="fa fa-envelope"></i>Email: <a href="mailto:info@afclinic.ru">info@afclinic.ru</a>
                    </address>

                    <!-- Social Links -->
                    <!--<ul class="social-icons">
                        <li><a href="#" data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                        <li><a href="#" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                        <li><a href="#" data-original-title="Goole Plus" class="rounded-x social_googleplus"></a></li>
                        <li><a href="#" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                    </ul>-->
                    <!-- End Social Links -->
                </div>
                <!-- End Address -->
                <!-- Latest Tweets -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="latest-tweets">
                        <div class="headline"><h2 class="heading-sm">График работы центра</h2></div>

                            <i class="fa fa-clock-o"></i> <b>Понедельник- пятница: с 9:00 до 20:00</b> <br/>

                            <i class="fa fa-clock-o"></i> <b>Суббота: с 10:00 до 18:00</b><br/>

                            <i class="fa fa-clock-o"></i> <b>Воскресенье: с 10:00 до 16:00</b><br/>


                    </div>
                </div>
                <!-- End Latest Tweets -->


            </div>
        </div>
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <p class="text-center"><?=date("Y");?> &copy; All Rights Reserved. <a href="/">Advanced Fertility Clinic</a></p>
        </div>
    </div><!--/copyright-->
</div>
<!--=== End Footer v2 ===-->
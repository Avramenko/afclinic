<?php $this->beginContent('//layouts/base'); ?>

<div class="container content">
    <div class="row">
        <div class="col-md-9">
            <?= $content; ?>
        </div>
        <div class="col-md-3">
            <div class="panel panel-purple">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-tasks"></i> Поделиться</h3>
                </div>
                <div class="panel-body">
                    <div style="margin:20px 0px;" class="ya-share2" data-services="whatsapp,odnoklassniki,vkontakte,facebook,moimir,linkedin,viber"></div>

                </div>
            </div>


            <?php $this->widget(
                "application.modules.contentblock.widgets.ContentBlockWidget",
                array("code" => "reklamnyy-blok-sprava-na-tekstovyh-stranicah"));
            ?>
            <div class="panel panel-purple">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-tasks"></i> Полезно знать!</h3>
                </div>
                <div class="panel-body">
                    <?php $this->widget(
                        'application.modules.menu.widgets.MenuWidget', array(
                            'name'         => 'polezno-znat',
                            'layout'       => 'top-menu',
                            /*'params'       => array('hideEmptyItems' => true),
                            'layoutParams' => array(
                                'htmlOptions' => array(
                                    'class' => 'jqueryslidemenu',
                                    'id'    => 'myslidemenu',
                                )
                            ),*/
                        )
                    ); ?>

                </div>
            </div>
            <!-- sidebar -->
        </div>

    </div>
</div>
<?php $this->endContent(); ?>

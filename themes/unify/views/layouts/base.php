<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>

    <?php Yii::app()->getController()->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        array(
            'httpEquivs' => array(
                'Content-Type' => 'text/html; charset=utf-8',
                'X-UA-Compatible' => 'IE=edge,chrome=1',
                'Content-Language' => Yii::app()->language
            ),
            'defaultTitle' => $this->yupe->siteName,
            'defaultDescription' => $this->yupe->siteDescription,
            'defaultKeywords' => $this->yupe->siteKeyWords,
        )
    ); ?>
    <!-- Web Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">
    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    /*Project CSS*/
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/flags.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/yupe.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/bootstrap/css/bootstrap.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/headers/header-v6.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/footers/footer-v2.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/animate.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/line-icons/line-icons.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/font-awesome/css/font-awesome.min.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/parallax-slider/css/parallax-slider.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/revolution-slider/rs-plugin/css/settings.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-colors/purple.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/custom.css');

    /*Project JS*/
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/jquery/jquery-migrate.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/back-to-top.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/smoothScroll.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/modernizr.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/jquery.cslider.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.js');

    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/blog.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap-notify.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');

    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/custom.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/app.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/owl-carousel.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/revolution-slider.js');
    
    //Yii::app()->getClientScript()->registerScriptFile('//cdn.jeapie.com/jeapiejs/780bc3cabd27a2b77ea188aae6b5fc57', CClientScript::POS_END);
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/parallax-slider.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/respond.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/html5shiv.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/placeholder-IE-fixes.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    Yii::app()->getClientScript()->registerScript('AppInit',"
        jQuery(document).ready(function() {
            App.init();
            OwlCarousel.initOwlCarousel();
            RevolutionSlider.initRSfullWidth();
            //ContactPage.initMap();
            //ContactPage.initPanorama();
        });
    ",CClientScript::POS_END)
    ?>




    <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>


    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script>
    <meta name='yandex-verification' content='614d7989c9f58fcd' />
    <meta name="google-site-verification" content="xN0CP9aCtoNFywsrqAzID_jC5F7XQ44f33gq728Ys4o" />
    <style>
        p, li, li a, label {
            font-size:16px;
        }
    </style>
</head>

<body class="header-fixed header-fixed-space">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32691945 = new Ya.Metrika({
                    id:32691945,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32691945" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Yandex.Metrika counter -->
<script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<script type="text/javascript">
    try {
        var yaCounter857560 = new Ya.Metrika({
            id:857560,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    } catch(e) { }
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/857560" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63957382-1', 'auto');
    ga('send', 'pageview');

</script>


    <div class="wrapper">
        <!--=== Header v6 ===-->
        <div class="header-v6 header-classic-white header-sticky">
            <!-- Navbar -->
            <div class="navbar mega-menu" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Navbar Brand -->
                        <div class="navbar-brand">
                            <a href="/">
                                <img class="shrink-logo" src="<?php echo $mainAssets?>/img/logo.png" alt="Logo">
                            </a>
                        </div>
                        <!-- ENd Navbar Brand -->

                        <!-- Header Inner Right -->
                        <div class="header-inner-right">
                            <ul class="menu-icons-list">

                                <li class="menu-icons">
                                    <i class="menu-icons-style search search-close search-btn fa fa-search"></i>
                                    <div class="search-open">
                                        <form action="/search" method="get">
                                        <input
                                            name="q"
                                            type="text"
                                            class="animated fadeIn form-control"
                                            placeholder="Поиск..."
                                            autocomplete="off">
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- End Header Inner Right -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <div class="menu-container">
                            <?php if (Yii::app()->hasModule('menu')): ?>
                                <?php $this->widget(
                                    'application.modules.menu.widgets.MenuWidget', [
                                        'name' => 'top-menu',
                                        'layout' => 'top-menu'
                                    ]
                                ); ?>
                            <?php endif; ?>
                        </div>
                    </div><!--/navbar-collapse-->
                </div>
            </div>
            <!-- End Navbar -->
        </div>
        <!--=== End Header v6 ===-->
        <?php /*CVarDumper::dump(Yii::app()->getRequest()->requestUri);*/?>
        <?php if(Yii::app()->getRequest()->requestUri == '/'){?>
            <?php $this->widget(
                "application.modules.contentblock.widgets.ContentBlockWidget",
                array(
                    "code" => "slayder-na-glavnoy-stranichke",
                    'silent' => true
                ));
            ?>
            <?php $this->widget("DoctorBlockWidget");
            ?>
        <?php }else{ ?>
            <!-- breadcrumbs -->
            <div class="breadcrumbs">
                <div class="container">
                    <h1 class="pull-left"><?php echo end($this->breadcrumbs);?></h1>
                    <?php $this->widget(
                        'bootstrap.widgets.TbBreadcrumbs',
                        [
                            'links' => $this->breadcrumbs,
                            'htmlOptions' => [
                                'class' => 'pull-right breadcrumb'
                            ]
                        ]
                    );?>
                </div>
            </div>
        <?php } ?>


        <?php //container content row margin-bottom-30 col-md-12 headline h2?>
        <?= $content; ?>

        <!-- footer -->
        <?php $this->renderPartial('//layouts/_footer'); ?>
        <!-- footer end -->


    </div>
</body>
</html>
<?php $this->beginContent('//layouts/base'); ?>
<div class="container">
    <div id="content">
        <?= $content; ?>
    </div>
    <!-- content -->
</div>
<?php $this->endContent(); ?>

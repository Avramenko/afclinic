<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 10.04.2016
 * Time: 3:08
 */
class DoctorController extends \yupe\components\controllers\FrontController
{
    /**
     * Объявляем действия:
     *
     * @return mixed actions
     **/
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yupe\components\actions\YCaptchaAction',
                'backColor' => 0xFFFFFF,
            ],
        ];
    }
    
    public function actionIndex()
    {
        $model = new Doctor('search');
        $model->unsetAttributes(); // clear any default values
        $model->status = DoctorStatusHelper::STATUS_HIRED;
        if (isset($_GET['Doctor']))
            $model->attributes = $_GET['Doctor'];
        $this->render('index', array('model' => $model));
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer Doctor нужной модели
     *
     * @return void
     * @throws CHttpException 404
     */
    public function loadModel($id)
    {
        $model = Doctor::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('doctor', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
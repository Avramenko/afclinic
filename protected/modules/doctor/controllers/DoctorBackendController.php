<?php
/**
 * Класс DoctorBackendController:
 *
 *   @category Yupeyupe\components\controllers\BackController
 *   @package  yupe
 *   @author   Yupe Team
<team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
use yupe\components\controllers\BackController;
class DoctorBackendController extends BackController
{
    /**
     * Отображает Специалиста по указанному идентификатору
     *
     * @param integer $id Идинтификатор Специалиста для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Создает новую модель Специалиста.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Doctor;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Doctor'])) {
            $model->attributes = $_POST['Doctor'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('doctor', 'Запись добавлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Редактирование Специалиста.
     *
     * @param integer $id Идинтификатор Специалиста для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Doctor'])) {
            $model->attributes = $_POST['Doctor'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('doctor', 'Запись обновлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Удаляет модель Специалиста из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор Специалиста, который нужно удалить
     *
     * @return void
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
// поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('doctor', 'Запись удалена!')
            );

// если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        } else
            throw new CHttpException(400, Yii::t('doctor', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
     * Управление Специалистами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Doctor('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Doctor']))
            $model->attributes = $_GET['Doctor'];
        $this->render('index', array('model' => $model));
    }


    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     * @throws CHttpException 404
     */
    public function loadModel($id)
    {
        $model = Doctor::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('doctor', 'Запрошенная страница не найдена.'));

        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(Doctor $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'doctor-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'Doctor',
                'validAttributes' => [
                    'status',
                ]
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'Doctor',
                'attribute' => 'sort'
            ]
        ];
    }
}

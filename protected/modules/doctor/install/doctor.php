<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 03.04.2016
 * Time: 21:39
 */
return [
    'module'    => [
        'class' => 'application.modules.doctor.DoctorModule',
    ],
    'import' => [
        'application.modules.doctor.helpers.*',
        'application.modules.doctor.widgets.*',
    ],
    'rules'     => [
        '/doctor/<id>' => 'doctor/doctor/view',
        '/doctor/' => 'doctor/doctor/index',
    ],
];
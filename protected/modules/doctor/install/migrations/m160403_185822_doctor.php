<?php

class m160403_185822_doctor extends yupe\components\DbMigration
{
	/*public function up()
	{
	}

	public function down()
	{
		echo "m160403_185822_doctor does not support migration down.\n";
		return false;
	}*/


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(
            '{{doctor_doctor}}',
            [
                'id' => 'pk ',
                'status' => 'int null',
                'first_name' => 'varchar(50) null',
                'last_name' => 'varchar(50) null',
                'middle_name' => 'varchar(50) null',
                'position' => 'varchar(255) null',
                'description' => 'text null',
                'photo' => 'varchar(255) null',
                'color' => 'varchar(255) null',
                'create_user_id' => 'integer DEFAULT NULL',
                'create_time' => 'datetime DEFAULT NULL',
                'update_user_id' => 'integer DEFAULT NULL',
                'update_time' => 'datetime DEFAULT NULL',
                'sort' => "integer NOT NULL DEFAULT '1'"
            ],
            $this->getOptions()
        );
        $this->createIndex("ix_{{doctor_doctor}}_create_user_id", '{{doctor_doctor}}', "create_user_id");
        $this->createIndex("ix_{{doctor_doctor}}_update_user_id", '{{doctor_doctor}}', "update_user_id");

        $this->addForeignKey(
            "fk_{{doctor_doctor}}_create_user",
            '{{doctor_doctor}}',
            'create_user_id',
            '{{user_user}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );

        $this->addForeignKey(
            "fk_{{doctor_doctor}}_update_user",
            '{{doctor_doctor}}',
            'update_user_id',
            '{{user_user}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
	}

	public function safeDown()
	{
		$this->dropTableWithForeignKeys('{{doctor_doctor}}');
	}

}
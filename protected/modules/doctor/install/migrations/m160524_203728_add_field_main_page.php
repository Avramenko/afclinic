<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 25.05.2016
 * Time: 23:49
 */

class m160524_203728_add_field_main_page extends CDbMigration
{
    /*public function up()
    {
    }

    public function down()
    {
        echo "m160524_203728_add_field_main_page does not support migration down.\n";
        return false;
    }*/


    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        echo "    >  Start m160524_203728_add_field_main_page mirgation up and clear schema cache...\n";
        $time=microtime(true);

        $this->addColumn('{{doctor_doctor}}', 'main_page', 'integer NOT NULL  DEFAULT 0');

        /**
         * Refresh all tables schemas cache
         * Обновление в кэше описаний схем всех таблиц
         */
        Yii::app()->db->schema->getTables(); // load all tables
        Yii::app()->db->schema->refresh(); // remove all loaded from cache
        Yii::app()->db->schema->getTables(); // load all tables again and store into cache

        echo " Done (time: ".sprintf('%.3f', microtime(true)-$time)."s)\n";
    }

    public function safeDown()
    {
        echo "    >  Start m160524_203728_add_field_main_page mirgation up and clear schema cache...\n";
        $time=microtime(true);
        $this->dropColumn('{{doctor_doctor}}', 'main_page');

        /**
         * Refresh all tables schemas cache
         * Обновление в кэше описаний схем всех таблиц
         */
        Yii::app()->db->schema->getTables(); // load all tables
        Yii::app()->db->schema->refresh(); // remove all loaded from cache
        Yii::app()->db->schema->getTables(); // load all tables again and store into cache

        echo " Done (time: ".sprintf('%.3f', microtime(true)-$time)."s)\n";
    }

}
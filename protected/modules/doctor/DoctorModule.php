<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 03.04.2016
 * Time: 21:24
 */
use yupe\components\WebModule;
class DoctorModule extends WebModule
{
    const VERSION = '0.1';

    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;
    public $allowedExtensions = 'jpg,jpeg,png,gif';

    public $uploadPath = 'doctor';

    public function getDependencies(){
        return [
            'user',
            'category',
            'image',
            'comment'
        ];
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getName()
    {
        return 'Специалисты';
    }

    public function getDescription()
    {
        return 'Специалисты клиники';
    }

    public function getAuthor()
    {
        return 'maxim avramenko';
    }

    public function getAuthorEmail()
    {
        return 'maxim.avramenko@gmail.com';
    }

    public function getUrl()
    {
        return 'http://site.ru';
    }

    public function getAdminPageLink()
    {
        return '/doctor/doctorBackend/index';
    }

    public function getIcon()
    {
        return "fa fa-fw fa-pencil";
    }

    public function getCategory()
    {
        return 'Специалисты';
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return false;
    }

    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'doctor.models.*',

            ]
        );
    }

    public function getNavigation()
    {
        return [

            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => 'Список',
                'url'   => ['/doctor/doctorBackend/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => 'Добавить',
                'url'   => ['/doctor/doctorBackend/create']
            ],
        ];
    }

}
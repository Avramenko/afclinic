<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('doctor')->getCategory() => array(),
        Yii::t('doctor', 'Специалисты') => array('/doctor/doctorBackend/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('doctor', 'Специалисты - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('doctor', 'Управление Специалистами'), 'url' => array('/doctor/doctorBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('doctor', 'Добавить Специалиста'), 'url' => array('/doctor/doctorBackend/create')),
        array('label' => Yii::t('doctor', 'Специалист') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('doctor', 'Редактирование Специалиста'), 'url' => array(
            '/doctor/doctorBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('doctor', 'Просмотреть Специалиста'), 'url' => array(
            '/doctor/doctorBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('doctor', 'Удалить Специалиста'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/doctor/doctorBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('doctor', 'Вы уверены, что хотите удалить Специалиста?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('doctor', 'Просмотр') . ' ' . Yii::t('doctor', 'Специалиста'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'status',
        'first_name',
        'last_name',
        'middle_name',
        'position',
        'description',
        'photo',
        'color',
        'create_user_id',
        'create_time',
        'update_user_id',
        'update_time',
        'sort',
),
)); ?>
<div class="comments-section">

    <?php $this->widget('application.modules.comment.widgets.CommentsWidget', [
        'redirectTo' => Yii::app()->createUrl('/doctor/doctor/view/'.$model->id),
        'model' => $model,
    ]); ?>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#post img').addClass('img-responsive');
        $('pre').each(function (i, block) {
            hljs.highlightBlock(block);
        });
    });
</script>
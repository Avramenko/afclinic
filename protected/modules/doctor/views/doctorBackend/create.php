<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('doctor')->getCategory() => array(),
        Yii::t('doctor', 'Специалисты') => array('/doctor/doctorBackend/index'),
        Yii::t('doctor', 'Добавление'),
    );

    $this->pageTitle = Yii::t('doctor', 'Специалисты - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('doctor', 'Управление Специалистами'), 'url' => array('/doctor/doctorBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('doctor', 'Добавить Специалиста'), 'url' => array('/doctor/doctorBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('doctor', 'Специалисты'); ?>
        <small><?php echo Yii::t('doctor', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
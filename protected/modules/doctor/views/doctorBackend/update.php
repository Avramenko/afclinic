<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('doctor')->getCategory() => array(),
        Yii::t('doctor', 'Специалисты') => array('/doctor/doctorBackend/index'),
        $model->id => array('/doctor/doctorBackend/view', 'id' => $model->id),
        Yii::t('doctor', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('doctor', 'Специалисты - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('doctor', 'Управление Специалистами'), 'url' => array('/doctor/doctorBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('doctor', 'Добавить Специалиста'), 'url' => array('/doctor/doctorBackend/create')),
        array('label' => Yii::t('doctor', 'Специалист') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('doctor', 'Редактирование Специалиста'), 'url' => array(
            '/doctor/doctorBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('doctor', 'Просмотреть Специалиста'), 'url' => array(
            '/doctor/doctorBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('doctor', 'Удалить Специалиста'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/doctor/doctorBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('doctor', 'Вы уверены, что хотите удалить Специалиста?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('doctor', 'Редактирование') . ' ' . Yii::t('doctor', 'Специалиста'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
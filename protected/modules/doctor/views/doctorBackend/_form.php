<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Doctor
 *   @var $form TbActiveForm
 *   @var $this DoctorBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'doctor-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('doctor', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('doctor', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?= $form->dropDownListGroup($model, 'status', [
                'widgetOptions' => [
                    'data' => DoctorStatusHelper::getList(),
                    
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->switchGroup($model, 'main_page',
                array(
                    'widgetOptions' => array(
                        'events'=>array(
                            'switchChange'=>'js:function(event, state) {
						  console.log(this); // DOM element
						  console.log(event); // jQuery event
						  console.log(state); // true | false
						}'
                        )
                    )
                )
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'last_name', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('last_name'), 'data-content' => $model->getAttributeDescription('last_name'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'first_name', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('first_name'), 'data-content' => $model->getAttributeDescription('first_name'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'middle_name', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('middle_name'), 'data-content' => $model->getAttributeDescription('middle_name'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'position', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('position'), 'data-content' => $model->getAttributeDescription('position'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textAreaGroup($model, 'description', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('description'), 'data-content' => $model->getAttributeDescription('description'))))); ?>
        </div>
    </div>
    <div class='row'>
        <div class="col-sm-7">
            <?php if (!$model->isNewRecord) : { ?>
                <?php echo CHtml::image($model->getImageUrl(300, 200), $model->imageAlt, ["width" => 300, "height" => 200]); ?>
            <?php } endif; ?>
            <?php echo $form->fileFieldGroup(
                $model,
                'photo',
                [
                    'widgetOptions' => [
                        'htmlOptions' => ['style' => 'background-color: inherit;'],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">

            <?php echo $form->colorpickerGroup(
                $model,
                'color',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5'
                    ),
                    'hint' => 'Укажите цвет для маркеровки специалиста',
                )
            ); ?>
        </div>
    </div>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('doctor', 'Сохранить Специалиста и продолжить'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('doctor', 'Сохранить Специалиста и закрыть'),
        )
    ); ?>

<?php $this->endWidget(); ?>
<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('doctor')->getCategory() => array(),
        Yii::t('doctor', 'Специалисты') => array('/doctor/doctorBackend/index'),
        Yii::t('doctor', 'Управление'),
    );

    $this->pageTitle = Yii::t('doctor', 'Специалисты - управление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('doctor', 'Управление Специалистами'), 'url' => array('/doctor/doctorBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('doctor', 'Добавить Специалиста'), 'url' => array('/doctor/doctorBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('doctor', 'Специалисты'); ?>
        <small><?php echo Yii::t('doctor', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('doctor', 'Поиск Специалистов');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('doctor-grid', {
            data: $(this).serialize()
        });

        return false;
    });
");
$this->renderPartial('_search', array('model' => $model));
?>
</div>

<br/>

<p> <?php echo Yii::t('doctor', 'В данном разделе представлены средства управления Специалистами'); ?>
</p>

<?php
 $this->widget('yupe\widgets\CustomGridView', array(
'id'           => 'doctor-grid',
'type'         => 'striped condensed',
'dataProvider' => $model->search(),
'filter'       => $model,
'sortableRows' => true,
'sortableAjaxSave' => true,
'sortableAttribute' => 'sort',
'sortableAction' => '/doctor/doctorBackend/sortable',
'columns'      => array(
    'id',
    [
        'name' => 'status',
        'class' => 'yupe\widgets\EditableStatusColumn',
        'url' => $this->createUrl('/doctor/doctorBackend/inline'),
        'source' => DoctorStatusHelper::getList(),
        'options' => DoctorStatusHelper::getStylesList(),
        'filter' => CHtml::activeDropDownList($model, 'status', DoctorStatusHelper::getList(), [
            'encode' => false,
            'empty' => '',
            'class' => 'form-control',
        ]),
    ],
    'last_name',
    'first_name',
    'middle_name',
    'position',
    'color',
    'main_page',
    /*[
        'name' => 'main_page',
        'class' => 'yupe\widgets\EditableStatusColumn',
        'url' => $this->createUrl('/doctor/doctorBackend/inline'),
        'source' => DoctorStatusHelper::getList(),
        'options' => DoctorStatusHelper::getStylesList(),
        'filter' => CHtml::activeDropDownList($model, 'main_page', DoctorStatusHelper::getList(), [
            'encode' => false,
            'empty' => '',
            'class' => 'form-control',
        ]),
    ],*/
    /*
    'photo',
    'description',
    'create_user_id',
    'create_time',
    'update_user_id',
    'update_time',
    'sort',
    */
array(
'class' => 'yupe\widgets\CustomButtonColumn',
),
),
)); ?>

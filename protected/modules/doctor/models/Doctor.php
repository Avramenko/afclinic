<?php

/**
 * This is the model class for table "{{doctor_doctor}}".
 *
 * The followings are the available columns in table '{{doctor_doctor}}':
 * @property integer $id
 * @property integer $status
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $position
 * @property string $description
 * @property string $photo
 * @property string $color
 * @property integer $create_user_id
 * @property string $create_time
 * @property integer $update_user_id
 * @property string $update_time
 * @property integer $sort
 * @property integer $main_page
 *
 * The followings are the available model relations:
 * @property User $createUser
 * @property User $updateUser
 */
Yii::import('application.modules.comment.components.ICommentable');
class Doctor extends yupe\models\YModel implements ICommentable
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{doctor_doctor}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, create_user_id, update_user_id, sort, main_page', 'numerical', 'integerOnly'=>true),
            array('first_name, last_name, middle_name', 'length', 'max'=>50),
			array('position, photo, color', 'length', 'max'=>255),
			array('description, create_time, update_time', 'safe'),
            //array('photo','required' ,'on' => 'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status, first_name, last_name, middle_name, position, description, photo, color, create_user_id, create_time, update_user_id, update_time, sort, main_page', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors()
	{
		$module = Yii::app()->getModule('doctor');

		return [
			'imageUpload' => [
				'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
				'attributeName' => 'photo',
				'minSize'       => $module->minSize,
				'maxSize'       => $module->maxSize,
				'types'         => $module->allowedExtensions,
				'requiredOn'    => 'insert',
				'uploadPath'    => $module->uploadPath,
			],
			'sortable'    => [
				'class'         => 'yupe\components\behaviors\SortableBehavior',
				'attributeName' => 'sort'
			],
			'seo'         => [
				'class'  => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
				'route'  => '/doctor/doctor/view',
				'params' => [
					'id' => function ($data) {
						return $data->id;
					}
				],
			],
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'create_time',
				'updateAttribute' => 'update_time',
			)
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createUser' => array(self::BELONGS_TO, 'User', 'create_user_id'),
			'updateUser' => array(self::BELONGS_TO, 'User', 'update_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Статус',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
			'position' => 'Должность',
			'description' => 'Описание',
			'photo' => 'Фотография',
			'color' => 'Цвет',
			'create_user_id' => 'Создал',
			'create_time' => 'Дата создания',
			'update_user_id' => 'Обновил',
			'update_time' => 'Дата обновления',
			'sort' => 'Сортировка',
			'main_page' => 'Показывать на главной странице',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status);
        $criteria->compare('first_name',$this->first_name,true);
        $criteria->compare('last_name',$this->last_name,true);
        $criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('description',$this->description,true);
		//$criteria->compare('photo',$this->photo,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('main_page',$this->main_page);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'     => ['defaultOrder' => 't.sort'],
			'pagination'=>['pageSize'=>$this->count()]
		));
	}

    /**
     * Условие для получения определённого количества записей:
     *
     * @param integer $count - количество записей
     *
     * @return self
     */
    public function limit($count = null)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'limit' => $count,
            ]
        );
        return $this;
    }
    /**
     * @return array
     */
    public function scopes()
    {
        return [

            'hired' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => DoctorStatusHelper::STATUS_HIRED],
            ],
            'fired'    => [
                'condition' => 't.status = :status',
                'params'    => [':status' => DoctorStatusHelper::STATUS_FIRED],
            ],
            'onHoliday' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => DoctorStatusHelper::STATUS_ON_HOLIDAY]
            ],
            'archive' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => DoctorStatusHelper::STATUS_ARCHIVE]
            ],
            'recent'    => [
                'order' => 'create_time DESC'
            ],
            'havePhoto' => [
                'condition' => 't.photo !=""'
            ],
			'onMainPage' => [
				'condition' => 't.main_page = 1'

			]
        ];
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Doctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->sort = Yii::app()->db->createCommand()
                ->select('MAX(sort) + 1')
                ->from($this->tableName())
                ->queryScalar();
        }

        return parent::beforeSave();
    }

	public function beforeValidate()
	{
		if ($this->getIsNewRecord()) {
			$this->create_user_id = Yii::app()->user->getId();
		}

		return parent::beforeValidate();
	}

    /**
     *
     */
    public function afterDelete()
    {
        Comment::model()->deleteAll(
            'model = :model AND model_id = :model_id',
            [
                ':model'    => 'Doctor',
                ':model_id' => $this->id
            ]
        );
        parent::afterDelete();
    }

	/**
	 * Получаем имя того, кто создал специалиста:
	 *
	 * @return string user full name
	 **/
	public function getCreateUserName()
	{
		return $this->createUser instanceof User
			? $this->createUser->getFullName()
			: '---';
	}

	/**
	 * Получаем имя того, кто обновил Специалиста:
	 *
	 * @return string user full name
	 **/
	public function getUpdateUserName()
	{
		return $this->updateUser instanceof User
			? $this->updateUser->getFullName()
			: '---';
	}

    /**
     * @return string fullName
     */
    public function getFullName()
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    /**
     * @return string shortName
     */
    public function getShortName()
    {
        return $this->last_name . ' ' . substr($this->first_name,0,2) . '. ' . substr($this->middle_name,0,2) . '.';
    }
	/**
	 * @return mixed|string
	 */
	public function getTitle()
	{
		return $this->getShortName(). ' - '.$this->position;
	}

	/**
	 * @return mixed
	 */
	public function getLink()
	{
		return Yii::app()->createAbsoluteUrl('/doctor/'.$this->id);
	}

    public function getImageAlt()
    {
        return $this->getFullName() . ' - ' .$this->position;
    }
}

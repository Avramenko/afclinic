<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 10.04.2016
 * Time: 1:37
 */
class DoctorStatusHelper
{
    const STATUS_FIRED = 0;
    const STATUS_HIRED = 1;
    const STATUS_ON_HOLIDAY = 2;
    const STATUS_ARCHIVE = 3;

    public static function getList()
    {
        return [
            self::STATUS_HIRED => 'В штате',
            self::STATUS_ON_HOLIDAY => 'В отпуске',
            self::STATUS_FIRED => 'Уволен',
            self::STATUS_ARCHIVE => 'Архив',
        ];
    }

    public static function getLabel($id)
    {
        $list = self::getList();

        return $list[$id];
    }

    public static function getStylesList()
    {
        return [
            self::STATUS_ARCHIVE => ['class' => 'label-default'],
            self::STATUS_FIRED => ['class' => 'label-danger'],
            self::STATUS_ON_HOLIDAY => ['class' => 'label-primary'],
            self::STATUS_HIRED=> ['class' => 'label-success'],
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 21.05.2016
 * Time: 3:16
 */

//CVarDumper::dump($models);
?>
<style>
    .team-v5 small:after{
        height: 0px;
    }
    /*@media (min-width: 1024px){
        .afc-height-290{
            height: 290px;
        }
    }*/
</style>
<div class="container content-sm">
    <div class="headline-center margin-bottom-60">
        <h2>Наши специалисты</h2>
        <p>Специалисты медицинского центра AFClinic проходили обучение, стажировку и работали в ведущих клиниках Австрии, Англии, Дании и Германии<br>
            Являются членами <b>РАРЧ</b> и <b>ESHRE</b>, Европейской ассоциации урологов <b>ЕАU</b> и <b>ESAU</b>.
        </p>
    </div>
    <div class="row team-v5 margin-bottom-30">
        <?php foreach ($models as $model){?>
            <?php echo $this->controller->renderFile(Yii::app()->basePath.'/modules/doctor/widgets/views/_view.php',['model'=>$model]);?>
        <?php } ?>
    </div>
        
    <!--/end row-->
</div>

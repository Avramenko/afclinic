<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 21.05.2016
 * Time: 3:13
 */
Yii::import('application.modules.doctor.models.Doctor');
Yii::import('application.modules.doctor.DoctorModule');
class DoctorBlockWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'index';

    /**
     * @throws CException
     */
    public function init()
    {
        
        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $this->render($this->view, ['models' => Doctor::model()->onMainPage()->findAll()]);
    }
}
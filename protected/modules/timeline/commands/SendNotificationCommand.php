<?php

/**
 * Class SendNotificationCommand
 */
class SendNotificationCommand extends CConsoleCommand {
    const WSDL = 'https://smsc.ru/sys/soap.php?wsdl';
    /**
     * @param array $args
     *
     */
    public function run($args)
    {
        // Достаем из базы события завтрашнего дня и отправляем каждому напоминание о визите к врачю
        //
        $today = '';
        $events = TimelineEvent::model()->active()->with(
            'updateUser',
            'createUser',
            'doctor',
            'patient')->findAll();
        CVarDumper::dump($events);
        //$client=new SoapClient(self::WSDL);
        //$ret = $client->send_sms(array('login'=>'avramenko', 'psw'=>'AvramenkoA1', 'phones'=>'79313549521', 'mes'=>'Hello world!', 'id'=>1, 'sender'=>'afclinic.ru', 'time'=>0));
        //$ret = $client->get_balance(array('login'=>'avramenko', 'psw'=>'AvramenkoA1'));
        //echo CVarDumper::dump($ret);
        echo "Достаем из базы события завтрашнего дня и отправляем каждому напоминание о визите к врачю\n";
    }

}

<?php
/**
 * Класс TimelineScheduleBackendController:
 *
 *   @category YupeController
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class TimelineScheduleBackendController extends yupe\components\controllers\BackController
{
    /**
     * Отображает График по указанному идентификатору
     *
     * @param integer $id Идинтификатор График для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        if(!Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
            $this->renderPartial('view',['model' => $this->loadModel($id)], false, true);
            Yii::app()->end();
        }
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Создает новую модель График.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new TimelineSchedule;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimelineSchedule'])) {
            $model->attributes = $_POST['TimelineSchedule'];

            if ($model->save()) {

                if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
                    header('Content-type: text/javascript');
                    echo $model->getScheduleJson();
                    Yii::app()->end();
                }


                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('timeline', 'Запись добавлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        if(!Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){

            if (isset($_GET['TimelineSchedule'])) {
                $model->attributes = $_GET['TimelineSchedule'];
            }

            $this->renderPartial('create',['model' => $model], false, true);
            Yii::app()->end();
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Редактирование График.
     *
     * @param integer $id Идинтификатор График для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimelineSchedule'])) {
            $model->attributes = $_POST['TimelineSchedule'];

            if ($model->save()) {

                if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
                    header('Content-type: text/javascript');
                    echo $model->getScheduleJson();
                    Yii::app()->end();
                }

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('timeline', 'Запись обновлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        if(!Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
            $this->renderPartial('update', array('model' => $model), false, true);
            Yii::app()->end();
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Удаляет модель График из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор График, который нужно удалить
     *
     * @return void
     * @throws CHttpException 400
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
// поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('timeline', 'Запись удалена!')
            );

// если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        } else
            throw new CHttpException(400, Yii::t('timeline', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
     * Управление Графиками.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new TimelineSchedule('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['TimelineSchedule']))
            $model->attributes = $_GET['TimelineSchedule'];
        $this->render('index', array('model' => $model));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     */
    public function loadModel($id)
    {
        $model = TimelineSchedule::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('timeline', 'Запрошенная страница не найдена.'));

        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(TimelineSchedule $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'timeline-schedule-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCopy($id)
    {
        $model = new TimelineSchedule;
        $schedule = $this->loadModel($id);

        if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest && isset($_POST['TimelineSchedule'])){

            header('Content-type: text/javascript');
            $model->attributes = $schedule->attributes;
            $model->attributes = $_POST['TimelineSchedule'];


            if($model->save()){
                echo $model->getScheduleJson();
                Yii::app()->end();
            }
        }

        Yii::app()->end();
    }

    public function actionDoctor($id = null)
    {
        if ($id === null)
            throw new CHttpException(404, Yii::t('timeline', 'Запрошенная страница не найдена.'));

        $schedules = TimelineSchedule::model()
        ->with(
            'updateUser',
            'createUser',
            'doctor')
        ->findAll([
            'condition' =>'doctor_id =:doctor_id AND start_time >=:start_time AND end_time <=:end_time',
            'params' => [
                'doctor_id' => $id,
                'start_time' => $_GET['start'],
                'end_time' => $_GET['end']

            ]
        ]);
        $json = [];
        foreach($schedules as $schedule){
            $json[] = $schedule->getArray('schedule');
        }
        echo CJSON::encode($json);
        Yii::app()->end();
    }


    public function actionGetAllJson()
    {
        $schedules = TimelineSchedule::model()
            ->with(
                'updateUser',
                'createUser',
                'doctor')
            ->findAll([
                'condition' =>'start_time >=:start_time AND end_time <=:end_time',
                'params' => [
                    'start_time' => $_GET['start'],
                    'end_time' => $_GET['end']

                ]
            ]);
        $this->renderPartial('json/schedules', compact('schedules'));
    }
}

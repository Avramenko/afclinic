<?php
/**
 * Класс TimelineEventBackendController:
 *
 *   @category YupeController
 *   @package  yupe
 *   @author   Yupe Team
<team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class TimelineEventBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Timeline.TimelineEventBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Timeline.TimelineEventBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Timeline.TimelineEventBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Timeline.TimelineEventBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Timeline.TimelineEventBackend.Delete']],
            ['deny']
        ];
    }


    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'TimelineEvent',
                'validAttributes' => [
                    'doctor_id',
                    'patient_id',
                    'visit',
                    'comment',
                    'description',
                    'source',
                    'status',
                ]
            ]
        ];
    }

    /**
     * Отображает Событие по указанному идентификатору
     *
     * @param integer $id Идинтификатор Событие для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        if(!Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
            $this->renderPartial('view',['model' => $this->loadModel($id)], false, true);
            Yii::app()->end();
        }
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Создает новую модель События.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new TimelineEvent;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimelineEvent'])) {
            $model->attributes = $_POST['TimelineEvent'];

            if ($model->save()) {
                /*$client=new SoapClient('https://smsc.ru/sys/soap.php?wsdl');
                $ret = $client->send_sms(array('login'=>'avramenko', 'psw'=>'AvramenkoA1', 'phones'=>'79313549521', 'mes'=>'Hello world!', 'id'=>$model->id, 'sender'=>'afclinic.ru', 'time'=>0));
                echo CVarDumper::dump($ret);

                Yii::app()->end();*/
                if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
                    header('Content-type: text/javascript');
                    echo $model->getCalendarJsObject();
                    Yii::app()->end();
                }
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('timeline', 'Запись добавлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }

        if(!Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){

            if (isset($_GET['TimelineEvent'])) {
                $model->attributes = $_GET['TimelineEvent'];
            }

            $this->renderPartial('create',['model' => $model], false, true);
            Yii::app()->end();
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Редактирование События.
     *
     * @param integer $id Идинтификатор Событие для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TimelineEvent'])) {
            $model->attributes = $_POST['TimelineEvent'];

            if ($model->save()) {

                if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
                    header('Content-type: text/javascript');
                    echo CJSON::encode($model->getArray());
                    Yii::app()->end();
                }

                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('timeline', 'Запись обновлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        if(!Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest){
            $this->renderPartial('update', array('model' => $model), false, true);
            Yii::app()->end();
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Удаляет модель События из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор События, который нужно удалить
     *
     * @return void
     * @throws CHttpException 400
     */
    public function actionDelete($id)
    {
        echo CVarDumper::dump(Yii::app()->getRequest()->getIsPostRequest());
        Yii::app()->end();
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('timeline', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        } else
            throw new CHttpException(400, Yii::t('timeline', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
     * Управление Событиями.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new TimelineEvent('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['TimelineEvent']))
            $model->attributes = $_GET['TimelineEvent'];
        $this->render('index', array('model' => $model));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     */
    public function loadModel($id)
    {
        $model = TimelineEvent::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('timeline', 'Запрошенная страница не найдена.'));

        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(TimelineEvent $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'timeline-event-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     *
     */
    public function actionCalendar()
    {
        $model = new TimelineEvent('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['TimelineEvent'])) {
            $model->attributes = $_GET['TimelineEvent'];
        }
        //$this->render('scheduler', compact('model','doctors','events','user'));
        $this->render('calendar', compact('model'));
    }





    public function actionGetEventJson()
    {
        if (isset($_GET['start'],$_GET['end'])) {
            $events = TimelineEvent::model()
                ->with(
                    'updateUser',
                    'createUser',
                    'doctor',
                    'patient')
                ->findAll([
                    'condition' =>'start_time >=:start_time AND end_time <=:end_time',
                    'params' => [
                        'start_time' => $_GET['start'],
                        'end_time' => $_GET['end']
                    ]
                ]);

            header('Content-Type: application/json');

            echo $this->eventJs($events);
            Yii::app()->end();
        }
    }

    protected function eventJs($events)
    {
        $json = [];

        $schedules = TimelineSchedule::model()
            ->with(
                'doctor'
            )
            ->findAll([
                'condition' =>'start_time >=:start_time AND end_time <=:end_time',
                'params' => [
                    'start_time' => $_GET['start'],
                    'end_time' => $_GET['end']
                ]
            ]);


        foreach($events as $event){
            if($event->status === 0){
                continue;
            }
            $json[] = [
                'id' => $event->id,
                'resourceId' => $event->doctor->id,
                'start' => $event->start_time,
                'end' => $event->end_time,
                'title' => $event->patient->getFullName(),
                //'className' => 'tooltip',
                'url' => Yii::app()->createUrl('/backend/timeline/timelineEvent/view/'.$event->id),
                'color' => $event->doctor->color
            ];
        }

        foreach($schedules as $schedule ){
            $json[] = $schedule->getEventScheduleArray();
        }
        return CJSON::encode($json);
    }



    public function actionDoctor($id)
    {
        if ($id === null)
            throw new CHttpException(404, Yii::t('timeline', 'Запрошенная страница не найдена.'));

        $events = TimelineEvent::model()
            ->with(
                'updateUser',
                'createUser',
                'doctor')
            ->findAll([
                'condition' =>'doctor_id =:doctor_id AND start_time >=:start_time AND end_time <=:end_time',
                'params' => [
                    'doctor_id' => $id,
                    'start_time' => $_GET['start'],
                    'end_time' => $_GET['end']

                ]
            ]);
        $json = [];
        foreach($events as $event){
            $json[] = $event->getArray('doctor');
        }
        echo CJSON::encode($json);
        Yii::app()->end();
    }

    public function actionCopy($id)
    {
        $model = new TimelineEvent;
        $event = $this->loadModel($id);

        if(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->request->isAjaxRequest && isset($_POST['TimelineEvent'])){

            header('Content-type: text/javascript');
            $model->attributes = $event->attributes;
            $model->attributes = $_POST['TimelineEvent'];


            if($model->save()){
                echo CJSON::encode($model->getArray());
                Yii::app()->end();
            }
        }

        Yii::app()->end();
    }

    public function actionGetAllJson()
    {
        $events = TimelineEvent::model()
            ->with(
                'updateUser',
                'createUser',
                'doctor')
            ->findAll([
                'condition' =>'start_time >=:start_time AND end_time <=:end_time',
                'params' => [
                    'start_time' => $_GET['start'],
                    'end_time' => $_GET['end']

                ]
            ]);
        $this->renderPartial('json/events', compact('events'));
    }
}

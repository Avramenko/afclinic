<?php
/**
 * Класс TimelineDoctorBackendController:
 *
 *   @category YupeController
 *   @package  yupe
 *   @author   Yupe Team
<team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class TimelineDoctorBackendController extends yupe\components\controllers\BackController
{
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'TimelineEvent',
                'validAttributes' => [
                    'user_id',
                    'position',
                    'description',
                    'color',

                ]
            ]
        ];
    }

    /**
     * Отображает Доктора по указанному идентификатору
     *
     * @param integer $id Идинтификатор Доктора для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Создает новую модель Доктора.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new TimelineDoctor;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TimelineDoctor'])) {
            $model->attributes = $_POST['TimelineDoctor'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('timeline', 'Запись добавлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Редактирование Доктора.
     *
     * @param integer $id Идинтификатор Доктора для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TimelineDoctor'])) {
            $model->attributes = $_POST['TimelineDoctor'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('timeline', 'Запись обновлена!')
                );

                if (!isset($_POST['submit-type']))
                    $this->redirect(array('update', 'id' => $model->id));
                else
                    $this->redirect(array($_POST['submit-type']));
            }
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Удаляет модель Доктора из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор Доктора, который нужно удалить
     *
     * @return void
     * @throws CHttpException 40
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('timeline', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        } else
            throw new CHttpException(400, Yii::t('timeline', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
     * Управление Докторами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new TimelineDoctor('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['TimelineDoctor']))
            $model->attributes = $_GET['TimelineDoctor'];
        $this->render('index', array('model' => $model));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     * @throws CHttpException 404
     */
    public function loadModel($id)
    {
        $model = TimelineDoctor::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('timeline', 'Запрошенная страница не найдена.'));

        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(TimelineDoctor $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'timeline-doctor-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetJson()
    {
        header('Content-Type: application/json');
        $doctors = TimelineDoctor::model()->findAll([
            'condition' => 'status=1'
        ]);
        $doctorsArray = [];
        foreach($doctors as $doctor){
            if($doctor->status === 0){
                continue;
            }
            $doctorsArray[] = [
                'id' => $doctor->id,
                //'title' => CHtml::link($doctor->fullName, Yii::app()->createUrl('/backend/timeline/timelineDoctor/view/'.$doctor->id)),
                'title' => $doctor->fullName,
                'url' => Yii::app()->createUrl('/backend/timeline/timelineDoctor/view/'.$doctor->id),
                //'background' => '#ff0000'
            ];
        }
        echo json_encode($doctorsArray);
        Yii::app()->end();
    }

    public function actionSchedule($id)
    {

        $model = $this->loadModel($id);
        $schedule = $this->newShedule($model);
        $this->render('schedule', compact('model','schedule'));

    }
    protected function newShedule($doctor)
    {
        $schedule = new TimelineSchedule;
        if(isset($_POST['ajax']) && $_POST['ajax']==='schedule-form')
        {
            echo CActiveForm::validate($schedule);
            Yii::app()->end();
        }
        if(isset($_POST['TimelineSchedule'])){
            $schedule->attributes=$_POST['TimelineSchedule'];
            if($doctor->addShedule($schedule))
            {
                header('Content-type: text/javascript');
                echo CJSON::encode(array(
                    'status'=>true,
                    'event' => $doctor->schedule->getSheduleJs,
                ));
                Yii::app()->end();
            }
        }
        return $schedule;
    }

    public function actionGetScheduleJson($id)
    {
        if (isset($_GET['start'],$_GET['end'])) {
            $schedules = TimelineSchedule::model()
                ->with(
                    'updateUser',
                    'createUser',
                    'doctor')
                ->findAll([
                    'condition' =>'doctor_id =:doctor_id AND start_time >=:start_time AND end_time <=:end_time',
                    'params' => [
                        'doctor_id' => $id,
                        'start_time' => $_GET['start'],
                        'end_time' => $_GET['end']

                    ]
                ]);

            header('Content-Type: application/json');

            echo $this->scheduleJson($schedules);
            //Yii::app()->end();
        }
        Yii::app()->end();
    }

    protected  function scheduleJson($schedules)
    {
        $json = [];
        foreach($schedules as $schedule){
            if($schedule->status === 0){
                continue;
            }
            $json[] = [
                'id' => $schedule->id,
                'title' => $schedule->getStatus(),
                'start'=> $schedule->start_time,
                'end'=> $schedule->end_time,
                'color' => $schedule->getStatusColor(),
                'overlap' => true,
            ];
        }
        return CJSON::encode($json);
    }


    public function actionGetEventJson($id)
    {
        if (isset($_GET['start'],$_GET['end'])) {
            $events = TimelineEvent::model()
                ->with(
                    'updateUser',
                    'createUser',
                    'doctor')
                ->findAll([
                    'condition' =>'doctor_id =:doctor_id AND start_time >=:start_time AND end_time <=:end_time',
                    'params' => [
                        'doctor_id' => $id,
                        'start_time' => $_GET['start'],
                        'end_time' => $_GET['end']

                    ]
                ]);
            $schedules = TimelineSchedule::model()
                ->with(
                    'updateUser',
                    'createUser',
                    'doctor')
                ->findAll([
                    'condition' =>'doctor_id =:doctor_id AND start_time >=:start_time AND end_time <=:end_time',
                    'params' => [
                        'doctor_id' => $id,
                        'start_time' => $_GET['start'],
                        'end_time' => $_GET['end']

                    ]
                ]);
            header('Content-Type: application/json');

            echo $this->eventsJson($events,$schedules);
            //CVarDumper::dump($this->eventsJson($events,$schedules));
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    protected function eventsJson($events,$schedules)
    {
        $json = [];
        foreach($events as $event){
            if($event->status === 0){
                continue;
            }
            $json[] = [
                'id' => $event->id,
                'title' => $event->getStatus(),
                'start'=> $event->start_time,
                'end'=> $event->end_time,
                'color' => $event->doctor->color
            ];
        }
        foreach($schedules as $schedule ){
            $json[] = $schedule->getEventScheduleArray();
        }
        return CJSON::encode($json);
    }

}

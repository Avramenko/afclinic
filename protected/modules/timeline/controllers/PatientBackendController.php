<?php
/**
 * Класс PatientBackendController:
 *
 *   @category YupeController
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class PatientBackendController extends yupe\components\controllers\BackController
{
    /**
     * Создает новую модель User.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        //$model = new PatientRegistrationForm();
        $model = new User('patient');
        //Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (($data = Yii::app()->getRequest()->getPost('User')) !== null) {

            $model->setAttributes($data);

            $model->setAttributes(
                [
                    'hash' => Yii::app()->userManager->hasher->hashPassword(
                        Yii::app()->userManager->hasher->generateRandomPassword()
                    ),
                ]
            );

            if(array_key_exists('email',$data) && !strlen($data['email'])){
                $model->setAttributes(
                    [
                        'email' => $model->nick_name.'@'.$_SERVER['SERVER_NAME']
                    ]
                );
            }

            if ($model->save()) {

                header('Content-type: text/javascript');
                echo CJSON::encode(array(
                    'status'=>true,
                    'scenario' => $model->scenario,
                    'validate' => $model->validate(),
                    'model' => $model,
                ));
                Yii::app()->end();

            }
        }
        $this->renderPartial('_form', array('model' => $model),false,true);
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param PatientRegistrationForm model, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(User $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'patient-registration-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
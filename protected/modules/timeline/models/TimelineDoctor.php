<?php

/**
 * This is the model class for table "{{timeline_doctor}}".
 *
 * The followings are the available columns in table '{{timeline_doctor}}':
 * @property integer $id
 * @property integer $status
 * @property integer $user_id
 * @property string $position
 * @property string $description
 * @property string $photo
 * @property string $color
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property User $user
 * @property TimelineEvent[] $events
 * @property TimelineSchedule[] $schedules
 */
class TimelineDoctor extends \yupe\models\YModel
{
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{timeline_doctor}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, position', 'required'),
			array('status, user_id', 'numerical', 'integerOnly'=>true),
			array('position, color, photo', 'length', 'max'=>255),
			array('description, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status, user_id, position, description, photo, color, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'events' => array(self::HAS_MANY, 'TimelineEvent', 'doctor_id'),
            'schedules' => array(self::HAS_MANY, 'TimelineSchedule', 'doctor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Статус',
			'user_id' => 'Пользователь в системе',
			'position' => 'Должность',
			'description' => 'Описание',
			'photo' => 'Фотография',
			'color' => 'Цвет',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TimelineDoctor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return array
     */
    public function behaviors()
    {
        if(PHP_SAPI=='cli')
            return [
                'CTimestampBehavior' => [
                    'class'             => 'zii.behaviors.CTimestampBehavior',
                    'setUpdateOnCreate' => true,
                ],
            ];

        //$module = Yii::app()->getModule('broadcast');
        return [
            'CTimestampBehavior' => [
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }


    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_ACTIVE],
            ],
            'fired'    => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_NOT_ACTIVE],
            ],
        ];
    }
    /**
     * Список возможных статусов пользователя:
     *
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('TimelineModule.timeline', 'Active'),
            self::STATUS_NOT_ACTIVE => Yii::t('TimelineModule.timeline', 'Fired'),
        ];
    }

    /**
     * Получение строкового значения
     * статуса пользователя:
     *
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status])
            ? $data[$this->status]
            : Yii::t('TimelineModule.timeline', 'status is not set');
    }
    public function beforeSave()
    {
        if($this->getIsNewRecord()){

        }else{

        }



        return parent::beforeSave();
    }

    public function getList()
    {
        return $this->active()->with('user')->findAll(['order' => 'user.last_name ASC']);
    }

    /**
     * Получаем полное имя доктора:
     *
     * @param string $separator - разделитель
     *
     * @return string
     */
    public function getFullName($separator = ' ')
    {
        return ($this->user->first_name || $this->user->last_name)
            ? $this->user->last_name . $separator . $this->user->first_name . ($this->user->middle_name ? ($separator . $this->user->middle_name) : "")
            : $this->user->nick_name;
    }

    public function addSchedule($schedule)
    {
        $schedule->doctor_id=$this->id;
        return $schedule->save();
    }
}

<?php

/**
 * This is the model class for table "{{timeline_message}}".
 *
 * The followings are the available columns in table '{{timeline_message}}':
 * @property integer $id
 * @property integer $type
 * @property string $mes
 * @property string $sender
 * @property string $time
 * @property string $query
 * @property string $cost
 * @property integer $cnt
 * @property integer $error
 * @property integer $err
 * @property integer $status
 * @property string $last_date
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property TimelineEvent[] $yupeTimelineEvents
 */
class TimelineMessage extends \yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{timeline_message}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type', 'required'),
            array('type, cnt, error, err, status', 'numerical', 'integerOnly'=>true),
            array('sender', 'length', 'max'=>255),
            array('cost', 'length', 'max'=>19),
            array('mes, time, query, last_date, create_time, update_time', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, mes, sender, time, query, cost, cnt, error, err, status, last_date, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'events' => array(self::MANY_MANY, 'TimelineEvent', '{{timeline_event_to_message}}(message_id, event_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'mes' => 'Mes',
            'sender' => 'Sender',
            'time' => 'Time',
            'query' => 'Query',
            'cost' => 'Cost',
            'cnt' => 'Cnt',
            'error' => 'Error',
            'err' => 'Err',
            'status' => 'Status',
            'last_date' => 'Last Date',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('type',$this->type);
        $criteria->compare('mes',$this->mes,true);
        $criteria->compare('sender',$this->sender,true);
        $criteria->compare('time',$this->time,true);
        $criteria->compare('query',$this->query,true);
        $criteria->compare('cost',$this->cost,true);
        $criteria->compare('cnt',$this->cnt);
        $criteria->compare('error',$this->error);
        $criteria->compare('err',$this->err);
        $criteria->compare('status',$this->status);
        $criteria->compare('last_date',$this->last_date,true);
        $criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TimelineMessage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
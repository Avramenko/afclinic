
<?php

/**
 * This is the model class for table "{{timeline_schedule}}".
 *
 * The followings are the available columns in table '{{timeline_schedule}}':
 * @property integer $id
 * @property integer $doctor_id
 * @property string $comment
 * @property integer $status
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $start_time
 * @property string $end_time
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property TimelineDoctor $doctor
 * @property User $updateUser
 * @property User $createUser
 */
class TimelineSchedule extends \yupe\models\YModel
{
    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{timeline_schedule}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('doctor_id', 'required'),
            array('doctor_id, status, create_user_id, update_user_id', 'numerical', 'integerOnly'=>true),
            array('comment, start_time, end_time, create_time, update_time', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, doctor_id, comment, status, create_user_id, update_user_id, start_time, end_time, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'updateUser' => array(self::BELONGS_TO, 'User', 'update_user_id'),
            'createUser' => array(self::BELONGS_TO, 'User', 'create_user_id'),
            'doctor' => array(self::BELONGS_TO, 'TimelineDoctor', 'doctor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'doctor_id' => 'Доктор',
            'comment' => 'Комментарий',
            'status' => 'Статус',
            'create_user_id' => 'Создал',
            'update_user_id' => 'Обновил',
            'start_time' => 'Дата начала',
            'end_time' => 'Дата окончания',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('doctor_id',$this->doctor_id);
        $criteria->compare('comment',$this->comment,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('create_user_id',$this->create_user_id);
        $criteria->compare('update_user_id',$this->update_user_id);
        $criteria->compare('start_time',$this->start_time,true);
        $criteria->compare('end_time',$this->end_time,true);
        $criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TimelineSchedule the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        if(PHP_SAPI=='cli')
            return [
                'CTimestampBehavior' => [
                    'class'             => 'zii.behaviors.CTimestampBehavior',
                    'setUpdateOnCreate' => true,
                ],
            ];

        //$module = Yii::app()->getModule('broadcast');
        return [
            'CTimestampBehavior' => [
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * Список возможных статусов пользователя:
     *
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('TimelineModule.timeline', 'Ведет прием пациентов'),
            self::STATUS_NOT_ACTIVE => Yii::t('TimelineModule.timeline', 'Не принимает пациентов'),
        ];
    }

    /**
     * Получение строкового значения
     * статуса пользователя:
     *
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status])
            ? $data[$this->status]
            : Yii::t('TimelineModule.timeline', 'status is not set');
    }
    /**
     * Список возможных цветов статусов графика работы доктора:
     *
     * @return array
     */
    public function getStatusColorList()
    {
        return [
            self::STATUS_ACTIVE => '#8fdf82',
            self::STATUS_NOT_ACTIVE => '#FF0000',
        ];
    }

    /**
     * Получение строкового цвета статуса
     * статуса графика доктора:
     *
     * @return string
     */
    public function getStatusColor()
    {
        $data = $this->getStatusColorList();

        return isset($data[$this->status])
            ? $data[$this->status]
            : '#d7d7d7';
    }


    public function beforeSave()
    {
        if($this->getIsNewRecord()){
            $this->create_user_id = Yii::app()->user->id;
        }else{
            $this->update_user_id = Yii::app()->user->id;
        }
        return parent::beforeSave();
    }

    public function getScheduleJson()
    {
        return CJSON::encode([
            'id' => $this->id,
            'title'=> $this->getStatus(),
            'start'=> $this->start_time,
            'end'=> $this->end_time,
            //'constraint'=> 'businessHours',
            'color' => $this->getStatusColor()
        ]);
    }

    public function getEventScheduleArray()
    {
        $arr = [
            'start'=> $this->start_time,
            'end'=> $this->end_time,
            'rendering'=> 'background',
            'color'=> $this->getStatusColor(),
            'resourceId' => $this->doctor_id,
        ];

        if(!$this->status){
            $arr = [
                'start'=> $this->start_time,
                'end'=> $this->end_time,
                'overlap'=> false,
                'rendering'=> 'background',
                'color'=> $this->getStatusColor(),
                'resourceId' => $this->doctor_id,

            ];
        }
        return $arr;
    }


    public function getArray($type)
    {
        $arr = [
            'start'=> $this->start_time,
            'end'=> $this->end_time,
            'color'=> $this->getStatusColor(),
        ];
        switch($type){
            case 'schedule':
                $arr['rendering'] = 'background';
                $arr['id'] = 'available';
                if(!$this->status){
                    $arr['id'] = 'notAvailable';
                    $arr['overlap'] = false;
                }
                break;
            case 'event':
                $arr = [
                    'id' => $this->id,
                    'title'=> $this->getStatus(),
                    'start'=> $this->start_time,
                    'end'=> $this->end_time,
                    'color' => $this->getStatusColor()
                ];
                break;
        }
        return $arr;
    }
}
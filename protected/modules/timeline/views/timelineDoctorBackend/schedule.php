<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$today = date("Y-m-d");
$doctorScheduleJsonUrl = Yii::app()->createUrl('/backend/timeline/timelineDoctor/getScheduleJson/'.$model->id);
$addScheduleUrl = Yii::app()->createUrl('/backend/timeline/timelineSchedule/create');
$updateScheduleUrl = Yii::app()->createUrl('/backend/timeline/timelineSchedule/update');
$copyScheduleUrl = Yii::app()->createUrl('/backend/timeline/timelineSchedule/copy');
$viewScheduleUrl = Yii::app()->createUrl('/backend/timeline/timelineSchedule/view');
$addPatientUrl = Yii::app()->createUrl('/backend/timeline/patient/create');
$tokenName = Yii::app()->getRequest()->csrfTokenName;
$token = Yii::app()->getRequest()->csrfToken;
$moduleAssets = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.timeline.views.assets')
);

//Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/css/style.css');
Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/lib/fullcalendar.min.css');
//Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/lib/fullcalendar.print.css');
Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/scheduler.css');
//Yii::app()->getClientScript()->registerScriptFile($moduleAssets.'/js/script.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/moment.min.js',CClientScript::POS_HEAD);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/fullcalendar.min.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/scheduler.min.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/lang/ru.js',CClientScript::POS_END);


$this->breadcrumbs = array(
    Yii::app()->getModule('timeline')->getCategory() => array(),
    Yii::t('timeline', 'Доктора') => array('/backend/timeline/timelineDoctor/index'),
    $model->id,
);

$this->pageTitle = Yii::t('timeline', 'Doctor Schedule') . $model->fullName;

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Докторами'), 'url' => array('/backend/timeline/timelineDoctor/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Доктора'), 'url' => array('/backend/timeline/timelineDoctor/create')),
    array('label' => Yii::t('timeline', 'Доктор') . ' «' . mb_substr($model->id, 0, 32) . '»'),
    array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование Доктора'), 'url' => array(
        '/backend/timeline/timelineDoctor/update/'.$model->id
    )),
    array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'График приема '), 'url' => array(
        '/backend/timeline/timelineDoctor/view/'.$model->id
    )),
);

Yii::app()->getClientScript()->registerScript('InitScheduler',
    <<<JS
    $(document).ready(function(){
        $('#calendar').fullCalendar({
            height: 'auto',
            businessHours: {
                start: '09:00', // a start time (10am in this example)
                end: '22:00', // an end time (6pm in this example)

                dow: [1, 2, 3, 4, 5, 6, 0]
                // days of week. an array of zero-based day of week integers (0=Sunday)
                // (Monday-Thursday in this example)
            },
            slotDuration: '00:15:00',
            snapDuration: '00:15:00',
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			now: '$today',
			selectable: true,
			//editable: true, // enable draggable events
			aspectRatio: 1.8,
			scrollTime: '09:00', // undo default 6am scrollTime
			minTime: '09:00:00',
            maxTime: '22:00:00',
            editable: true,
			header: {
				left: 'addUser, changeDoctor, addEvent today prev,next',
				center: 'title',
				//right: 'timelineDay,timelineThreeDays,agendaWeek,month'
				right: 'agendaDay,agendaWeek,month'
			},
            customButtons: {
			    addEvent : {
			        text : 'График приема пациентов',
			        click: function() {
                        window.location.href = '/backend/timeline/timelineDoctor/view/$model->id';
                    }
			    },
			    changeDoctor : {
			        text : 'Выбрать другого врача',
			        click: function() {
                        window.location.href = '/backend/timeline/timelineDoctor/';
                    }
			    },
			    addUser: {
                    text: 'Новый пациент',
                    click: function() {
                        $.ajax({
                                    type: "GET",
                                    url: "$addPatientUrl",
                                    //data: $("#review-comment-personalInformation").serialize(),
                                    success: function(data){
                                        $("#patient-registration-modal .modal-body").html(data);
                                        $("#patient-registration-modal").modal('show');

                                    }
                                });
                        //$('#patient-registration-modal').modal('show');
                    }
                }
            },
			defaultView: 'agendaWeek',
            views: {
                basic: {
                    // options apply to basicWeek and basicDay views
                },
                agenda: {
                    // options apply to agendaWeek and agendaDay views

                },
                week: {
                    // options apply to basicWeek and agendaWeek views
                },
                day: {
                    // options apply to basicDay and agendaDay views
                },
                timelineThreeWeek: {
					type: 'agendaWeek',
					duration: { days: 21 }
				}
            },
            slotLabelFormat : [
                //'D MMMM YYYY', // top level of text
                'HH:mm'        // lower level of text
            ],
			events: {
			    url : '$doctorScheduleJsonUrl',
			    error: function(e) {
					console.log('error');
					console.log(e);
				}
			},
			select: function(start, end, jsEvent, view, resource) {
			    console.log('Тыкнули в ячейку для выбора дня или времени дня');
			    if(view.name === 'month'){
			        $('#calendar').fullCalendar( 'changeView', 'agendaWeek' );
			        $('#calendar').fullCalendar( 'gotoDate', start.format() );
			        $("#calendar").fullCalendar("refetchEvents");
			    }
				console.log(
					'select callback',
					start.format(),
					end.format(),
					resource ? resource.id : '(no resource)'
				);
				if(view.name === 'agendaDay' || view.name === 'agendaWeek'){
                    console.log('Insert покажем форму для создания расписания врача');
                    $.ajax({
                        url : '$addScheduleUrl',
                        type : 'GET',
                        dataType : 'html',
                        data : {
                            TimelineSchedule : {
                                start_time : start.format(),
                                end_time : end.format(),
                                doctor_id : '$model->id'
                            }
                        },
                        success : function( data, textStatus, jqXHR){
                            // получили форму с сервера, показываем модальное окно
                            console.log('add schedule');
                            console.log(data);
                            //console.log(textStatus);
                            //console.log(jqXHR);
                            $('#schedule-modal .modal-body').html(data);
                            $('#schedule-modal').modal('show');
                        },
                        error : function(jqXHR, status, errorThrown ){
                            console.log(status);
                            console.log(errorThrown);
                        }
                    });

			    }
			},
			dayClick: function() {
			    // отрабатывает при любом нажатии на рабочую область календаря
                //alert('a day has been clicked!');
            },
            eventClick: function(calEvent, jsEvent, view) {
                // Выбрали событие для редактирования
                console.log('Update Выбрали событие для редактирования расписания врача');
                //console.log(calEvent);
                //console.log(jsEvent);
                //console.log(view);
            //console.log(this);
                $.ajax({
                    url : '$updateScheduleUrl/'+calEvent.id,
                    type : 'GET',
                    dataType : 'html',
                    success : function( data, textStatus, jqXHR){
                        console.log('Получили форму для редактирования');
                        $('#schedule-modal .modal-body').html(data);
                        $('#schedule-modal').modal('show');
                    },
                    error : function(jqXHR, status, errorThrown ){
                        console.log(status);
                        console.log(errorThrown);
                    }
                });


                if (calEvent.url) {
                    //console.log(calEvent);
                    //window.open(schedule.url);
                    return false;
                }

            },
            eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
                // Если зажали шифт то копируем событие
                $.ajax({
                    url : (jsEvent.shiftKey)? '$copyScheduleUrl/'+event.id : '$updateScheduleUrl/'+event.id,
                    type : 'POST',
                    dataType: 'json',
                    data : {
                        TimelineSchedule : {
                            start_time : event.start.format(),
                            end_time : event.end.format(),
                            doctor_id : '$model->id'
                        },
                        $tokenName : '$token'
                    },
                    success : function(data, status) {

                        console.log(data);
                        console.log(status);
                        if(jsEvent.shiftKey){
                            console.log(data.id);
                            //$("#calendar").fullCalendar( "removeEvents", [data.id] );
                            //$("#calendar").fullCalendar( "addEventSource", [data] );
                        }

                        $("#calendar").fullCalendar("refetchEvents");
                    },
                    error : function(data,status) {
                        revertFunc();
                    }
                });
                //alert(event.title + " was dropped on " + event.start.format());
                console.log(event.title + " was dropped on " + event.start.format());
                //console.log(event);
                //console.log(event.start);
                //console.log(jsEvent);
                //console.log(ui);
                //console.log(view);
                //console.log(jsEvent.shiftKey);

                /*if(jsEvent.shiftKey){
                    revertFunc();
                }*/
                /*if (!confirm("Are you sure about this change?")) {
                    revertFunc();
                }*/

            },
            eventResize : function( event, delta, revertFunc ) {
                console.log(event);
                console.log(delta);
                $.ajax({
                    url : '$updateScheduleUrl/'+event.id,
                    type : 'POST',
                    dataType: 'json',
                    data : {
                        TimelineSchedule : {
                            start_time : event.start.format(),
                            end_time : event.end.format()

                        },
                        $tokenName : '$token'
                    },
                    success : function(data,status){
                        $("#calendar").fullCalendar("refetchEvents");
                    }
                });
            },
            loading : function( isLoading, view ){
                console.log('isLoading: '+isLoading);
                console.log(view);
                if(isLoading){
                    $("#loading").addClass('loader in');
                }else{
                    $("#loading").removeClass('loader in');
                }
            }
		});

		/*$('#select-G').on('click', function() {
			$('#calendar').fullCalendar('select', '2015-08-07T02:00:00', '2015-08-07T07:00:00', 'g');
		});

		$('#select-unspecified').on('click', function() {
			$('#calendar').fullCalendar('select', '2015-08-07T02:00:00', '2015-08-07T07:00:00');
		});*/
		/*$.fn.popover({
                content : 'Привет!',
                placement : 'top'
        });*/
});
JS
    ,CClientScript::POS_END);


?>
<div id="patient-registration-modal" class="modal fade" role="dialog" aria-labelledby="userModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="userModalLabel">Добавление нового пациента</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="schedule-modal" class="modal fade" role="dialog" aria-labelledby="scheduleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="scheduleModalLabel">Расписание</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'График дежурства'); ?> : <?php echo $model->fullName; ?>

    </h1>
</div>
<div id="calendar"></div>
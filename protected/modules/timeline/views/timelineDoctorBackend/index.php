<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$today = date("Y-m-d");

$allSchedulesUrl = Yii::app()->createUrl('/backend/timeline/timelineSchedule/getAllJson');
$allEventsUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/getAllJson');

$eventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/getEventJson');
$sourceUrl = Yii::app()->createUrl('/backend/timeline/timelineDoctor/getJson');
$userCreateUrl = Yii::app()->createUrl('/backend/user/user/create');
$viewEventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/view/');
$addEventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/create');
$addPatientUrl = Yii::app()->createUrl('/backend/timeline/patient/create');


$updateEventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/update');
$copyEventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/copy');


$tokenName = Yii::app()->getRequest()->csrfTokenName;
$token = Yii::app()->getRequest()->csrfToken;

$moduleAssets = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.timeline.views.assets')
);

//Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/css/style.css');
Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/lib/fullcalendar.min.css');
//Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/lib/fullcalendar.print.css');
Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/scheduler.css');
//Yii::app()->getClientScript()->registerScriptFile($moduleAssets.'/js/script.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/moment.min.js',CClientScript::POS_HEAD);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/fullcalendar.min.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/scheduler.min.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/lang/ru.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScript('InitScheduler',
    <<<JS
    $(document).ready(function(){
        $('#calendar').fullCalendar({
            height: 'auto',
            businessHours: {
                start: '09:00', // a start time (10am in this example)
                end: '22:00', // an end time (6pm in this example)

                dow: [1, 2, 3, 4, 5, 6, 0]
                // days of week. an array of zero-based day of week integers (0=Sunday)
                // (Monday-Thursday in this example)
            },
            slotDuration: '00:15:00',
            snapDuration: '00:15:00',
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			now: '$today',
			selectable: true,
			//editable: true, // enable draggable events
			aspectRatio: 1.8,
			scrollTime: '09:00', // undo default 6am scrollTime
			minTime: '09:00:00',
            maxTime: '22:00:00',
            editable: true,
			header: {
				left: 'addUser, today prev,next',
				center: 'title',
				//right: 'timelineDay,timelineThreeDays,agendaWeek,month'
				right: 'timelineDay,timelineWeek,month'
			},
			//slotWidth : '150px',
			customButtons: {
			    addUser: {
                    text: 'Новый пациент',
                    click: function() {
                        $.ajax({
                                    type: "GET",
                                    url: "$addPatientUrl",
                                    success: function(data){
                                        $("#patient-registration-modal .modal-body").html(data);
                                        $("#patient-registration-modal").modal('show');
                                    }
                                });
                        //$('#patient-registration-modal').modal('show');
                    }
                }
            },
			defaultView: 'timelineWeek',
			views: {
                basic: {
                    // options apply to basicWeek and basicDay views
                },
                agenda: {
                    // options apply to agendaWeek and agendaDay views

                },
                week: {
                    // options apply to basicWeek and agendaWeek views
                },
                day: {
                    // options apply to basicDay and agendaDay views
                },
                timelineThreeWeek: {
					type: 'timelineWeek',
					duration: { days: 21 }
				}
            },
			//timeFormat: 'h:mm',
			slotLabelFormat : [
			    'Do MMMM YYYY', // top level of text
                'HH:mm'        // lower level of text
			],
			resourceRender: function(dataTds, eventTd) {
			    eventTd.css('cursor','pointer')
                //console.log(dataTds);
                //console.log(eventTd);
                eventTd.on('click', function(){
                    window.location.href = dataTds.url;
                });
            },
            /*resourceText : function(resource){
                console.log(resource);
                return '<a href="/backend/timeline/timelineDoctor/view/'+resource.id+'">'+resource.title+'</a>';
            },*/
			resourceAreaWidth : '250px',
			resourceLabelText: 'Специалисты',

			resources: { // you can also specify a plain string like 'json/resources.json'
				url: '$sourceUrl',
				error: function(e) {
					console.log('error');
					console.log(e);
				}
			},

            //eventLimit: true,

			eventSources: [
			    '$allEventsUrl',
			    '$allSchedulesUrl'
			],
			/*selectOverlap : function(event) {
                console.log('selectOverlap');
                return event.rendering === 'background';
            },*/
            //selectConstraint : 'available',
			select: function(start, end, jsEvent, view, resource) {
			    //console.log(view.name);
			    if(view.name === 'month'){
			        $('#calendar').fullCalendar( 'changeView', 'timelineDay' );
			        $('#calendar').fullCalendar( 'gotoDate', start.format() );
			    }
				console.log(
					'select callback',
					start.format(),
					end.format(),
					resource ? resource.id : '(no resource)'
				);
				if(view.name !== 'month'){
                    //console.log('покажем форму для записи на прием');
                    $.ajax({
                        url : '$addEventUrl',
                        type : 'GET',
                        dataType : 'html',
                        data : {
                            TimelineEvent : {
                                start_time : start.format(),
                                end_time : end.format(),
                                doctor_id : resource.id
                            }
                        },
                        success : function( data, textStatus, jqXHR){
                            //console.log('add event');
                            //console.log(data);
                            //console.log(textStatus);
                            //console.log(jqXHR);
                            $('#add-event-modal .modal-body').html(data);
                            $('#add-event-modal').modal('show');
                        },
                        error : function(jqXHR, status, errorThrown ){
                            console.log(status);
                            console.log(errorThrown);
                        }
                    });

			    }
			},
			dayClick: function() {
			    // отрабатывает при любом нажатии на рабочую область календаря
                //alert('a day has been clicked!');
            },
            eventClick: function(calEvent, jsEvent, view) {
                console.log(calEvent);
                console.log(jsEvent);
                console.log(view);
            //console.log(this);
                $.ajax({
                    url : '$viewEventUrl/'+calEvent.id,
                    type : 'GET',
                    dataType : 'html',
                    success : function( data, textStatus, jqXHR){
                        $('#view-event-modal .modal-body').html(data);
                        $('#view-event-modal').modal('show');
                    },
                    error : function(jqXHR, status, errorThrown ){
                        console.log(status);
                        console.log(errorThrown);
                    }
                });


                if (calEvent.url) {
                //console.log(calEvent);
                    //window.open(event.url);
                    return false;
                }
            },
            eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
                console.log(event.title + " was dropped on " + event.start.format());
                console.log(event);

                $.ajax({
                    url : (jsEvent.shiftKey)? '$copyEventUrl/'+event.id : '$updateEventUrl/'+event.id,
                    type : 'POST',
                    dataType: 'json',
                    data : {
                        TimelineEvent : {
                            start_time : event.start.format(),
                            end_time : event.end.format(),
                            doctor_id : event.resourceId
                        },
                        $tokenName : '$token'
                    },
                    success : function(data, status) {

                        console.log('получили ответ после обновления события');
                        console.log(data);
                        console.log(status);
                        if(jsEvent.shiftKey){
                            console.log(data.id);
                        }
                        $("#calendar").fullCalendar("refetchEvents");
                    },
                    error : function(data,status) {
                        console.log('произошла ошибка '+ status);
                        console.log(data);
                        revertFunc();
                    }
                });



            },
            eventResize : function( event, delta, revertFunc ) {
                $.ajax({
                    url : '$updateEventUrl/'+event.id,
                    type : 'POST',
                    dataType: 'json',
                    data : {
                        TimelineEvent : {
                            start_time : event.start.format(),
                            end_time : event.end.format(),
                            doctor_id : event.resourceId
                        },
                        $tokenName : '$token'
                    },
                    success : function(data,status){
                        $("#calendar").fullCalendar("refetchEvents");
                    }
                });
            },
            loading : function( isLoading, view ){
                //console.log('isLoading: '+isLoading);
                //console.log(view);
                if(isLoading){
                    $("#loading").addClass('loader in');
                }else{
                    $("#loading").removeClass('loader in');
                }
            }
		});

		/*$('#select-G').on('click', function() {
			$('#calendar').fullCalendar('select', '2015-08-07T02:00:00', '2015-08-07T07:00:00', 'g');
		});

		$('#select-unspecified').on('click', function() {
			$('#calendar').fullCalendar('select', '2015-08-07T02:00:00', '2015-08-07T07:00:00');
		});*/
		/*$.fn.popover({
                content : 'Привет!',
                placement : 'top'
        });*/
});
JS
    ,CClientScript::POS_END);


$this->breadcrumbs = array(
    Yii::app()->getModule('timeline')->getCategory() => array(),
    Yii::t('timeline', 'Доктора') => array('/backend/timeline/timelineDoctor/index'),
    Yii::t('timeline', 'Управление'),
);

$this->pageTitle = Yii::t('timeline', 'Доктора - управление');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Докторами'), 'url' => array('/backend/timeline/timelineDoctor/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Доктора'), 'url' => array('/backend/timeline/timelineDoctor/create')),
);
?>
<div id="patient-registration-modal" class="modal fade" role="dialog" aria-labelledby="userModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="userModalLabel">Карточка Па</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="add-event-modal" class="modal fade" role="dialog" aria-labelledby="addEventModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addEventModalLabel">Add Event</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="view-event-modal" class="modal fade" role="dialog" aria-labelledby="viewEventModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="viewEventModalLabel">View Event</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Доктора'); ?>
        <small><?php echo Yii::t('timeline', 'управление'); ?></small>
    </h1>
</div>

<div id="calendar"></div>

<pre>

</pre>
<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('timeline', 'Поиск Докторов');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('timeline-doctor-grid', {
            data: $(this).serialize()
        });

        return false;
    });
");
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<br/>

<p> <?php echo Yii::t('timeline', 'В данном разделе представлены средства управления Докторами'); ?>
</p>

<?php
$this->widget('yupe\widgets\CustomGridView', array(
    'id'           => 'timeline-doctor-grid',
    'type'         => 'striped condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'id',
        'status',
        [
            'class'    => 'bootstrap.widgets.TbEditableColumn',
            'editable' => [
                'url'    => $this->createUrl('/timeline/timelineDoctorBackend/inline'),
                'mode'   => 'popup',
                'type'   => 'select',
                'title'  => Yii::t(
                    'TimelineModule.timeline',
                    'Select {field}',
                    ['{field}' => mb_strtolower($model->getAttributeLabel('user_id'))]
                ),
                'source' => CHtml::listData(User::model()->findAll(), 'id', 'fullName'),
                'params' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                ]
            ],
            'name'     => 'user_id',
            'type'     => 'raw',
            'filter'   => CHtml::activeDropDownList(
                $model,
                'user_id',
                CHtml::listData(User::model()->findAll(), 'id', 'fullName'),
                ['class' => 'form-control', 'empty' => '']
            ),
        ],
        'position',
        'description',
        'photo',
        /*
        'create_time',
        'update_time',
        */
        array(
            'class' => 'yupe\widgets\CustomButtonColumn',
        ),
    ),
)); ?>
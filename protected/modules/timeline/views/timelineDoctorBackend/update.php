<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'Доктора') => array('/backend/timeline/timelineDoctor/index'),
        $model->id => array('/backend/timeline/timelineDoctor/view', 'id' => $model->id),
        Yii::t('timeline', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('timeline', 'Доктора - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Докторами'), 'url' => array('/backend/timeline/timelineDoctor/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Доктора'), 'url' => array('/backend/timeline/timelineDoctor/create')),
        array('label' => Yii::t('timeline', 'Доктор') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование Доктора'), 'url' => array(
            '/backend/timeline/timelineDoctor/update/'.$model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'Просмотреть Доктора'), 'url' => array(
            '/backend/timeline/timelineDoctor/view/'.$model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('timeline', 'Удалить Доктора'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/backend/timeline/timelineDoctor/delete', 'id' => $model->id),
            'confirm' => Yii::t('timeline', 'Вы уверены, что хотите удалить Доктора?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Редактирование') . ' ' . Yii::t('timeline', 'Доктора'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TimelineDoctor
 *   @var $form TbActiveForm
 *   @var $this TimelineDoctorBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'timeline-doctor-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => array('class' => 'well'),
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data'        => $model->getStatusList(),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content'        => $model->getAttributeDescription('status')
                        ],
                    ],
                ]
            ); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->select2Group(
                $model,
                'user_id',
                [
                    'widgetOptions' => [
                        'data' => ['' => '---'] + CHtml::listData(User::model()->getList(), 'id', 'fullName'),
                    ]
                ]
            ); ?>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'position', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('position'), 'data-content' => $model->getAttributeDescription('position'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textAreaGroup($model, 'description', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('description'), 'data-content' => $model->getAttributeDescription('description'))))); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">

            <?php echo $form->colorpickerGroup(
                $model,
                'color',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5'
                    ),
                    'hint' => 'Укажите цвет для маркеровки специалиста',
                )
            ); ?>
        </div>
    </div>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('timeline', 'Сохранить Доктора и продолжить'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('timeline', 'Сохранить Доктора и закрыть'),
        )
    ); ?>

<?php $this->endWidget(); ?>
<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'Доктора') => array('/backend/timeline/timelineDoctor/index'),
        Yii::t('timeline', 'Добавление'),
    );

    $this->pageTitle = Yii::t('timeline', 'Доктора - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Докторами'), 'url' => array('/backend/timeline/timelineDoctor/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Доктора'), 'url' => array('/backend/timeline/timelineDoctor/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Доктора'); ?>
        <small><?php echo Yii::t('timeline', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
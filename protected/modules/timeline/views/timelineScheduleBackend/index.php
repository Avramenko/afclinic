<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'Графики') => array('/backend/timeline/timelineSchedule/index'),
        Yii::t('timeline', 'Управление'),
    );

    $this->pageTitle = Yii::t('timeline', 'Графики - управление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Графиками'), 'url' => array('/backend/timeline/timelineSchedule/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить График'), 'url' => array('/backend/timeline/timelineSchedule/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Графики'); ?>
        <small><?php echo Yii::t('timeline', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('timeline', 'Поиск Графики');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('timeline-schedule-grid', {
            data: $(this).serialize()
        });

        return false;
    });
");
$this->renderPartial('_search', array('model' => $model));
?>
</div>

<br/>

<p> <?php echo Yii::t('timeline', 'В данном разделе представлены средства управления Графиками'); ?>
</p>

<?php
 $this->widget('yupe\widgets\CustomGridView', array(
'id'           => 'timeline-schedule-grid',
'type'         => 'striped condensed',
'dataProvider' => $model->search(),
'filter'       => $model,
'columns'      => array(
        'id',
        'doctor_id',
        'comment',
        'status',
        'create_user_id',
        'update_user_id',
        /*
        'start_time',
        'end_time',
        'create_time',
        'update_time',
        */
array(
'class' => 'yupe\widgets\CustomButtonColumn',
),
),
)); ?>

<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'Графики') => array('/backend/timeline/timelineSchedule/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('timeline', 'Графики - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Графиками'), 'url' => array('/backend/timeline/timelineSchedule/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить График'), 'url' => array('/backend/timeline/timelineSchedule/create')),
        array('label' => Yii::t('timeline', 'График') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование График'), 'url' => array(
            '/backend/timeline/timelineSchedule/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'Просмотреть График'), 'url' => array(
            '/backend/timeline/timelineSchedule/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('timeline', 'Удалить График'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/backend/timeline/timelineSchedule/delete', 'id' => $model->id),
            'confirm' => Yii::t('timeline', 'Вы уверены, что хотите удалить График?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Просмотр') . ' ' . Yii::t('timeline', 'График'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'doctor_id',
        'comment',
        'status',
        'create_user_id',
        'update_user_id',
        'start_time',
        'end_time',
        'create_time',
        'update_time',
),
)); ?>

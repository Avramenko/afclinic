<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
        Yii::t('timeline', 'Добавление'),
    );

    $this->pageTitle = Yii::t('timeline', 'События - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'События'); ?>
        <small><?php echo Yii::t('timeline', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
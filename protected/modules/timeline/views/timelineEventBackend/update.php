<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
        $model->id => array('/backend/timeline/timelineEvent/view', 'id' => $model->id),
        Yii::t('timeline', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('timeline', 'События - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
        array('label' => Yii::t('timeline', 'Событие') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование События'), 'url' => array(
            '/backend/timeline/timelineEvent/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'Просмотреть Событие'), 'url' => array(
            '/backend/timeline/timelineEvent/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('timeline', 'Удалить Событие'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/backend/timeline/timelineEvent/delete', 'id' => $model->id),
            'confirm' => Yii::t('timeline', 'Вы уверены, что хотите удалить Событие?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Редактирование') . ' ' . Yii::t('timeline', 'События'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
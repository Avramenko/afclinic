<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
        Yii::t('timeline', 'Управление'),
    );

    $this->pageTitle = Yii::t('timeline', 'События - управление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'События'); ?>
        <small><?php echo Yii::t('timeline', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('timeline', 'Поиск Событий');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('timeline-event-grid', {
            data: $(this).serialize()
        });

        return false;
    });
");
$this->renderPartial('_search', array('model' => $model));
?>
</div>

<br/>

<p> <?php echo Yii::t('timeline', 'В данном разделе представлены средства управления Событиями'); ?>
</p>

<?php
$this->widget('yupe\widgets\CustomGridView', array(
    'id'           => 'timeline-event-grid',
    'type'         => 'striped condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'id',
        'start_time',
        [
            'class'    => 'bootstrap.widgets.TbEditableColumn',
            'editable' => [
                'url'    => $this->createUrl('/timeline/timelineEventBackend/inline'),
                'mode'   => 'popup',
                'type'   => 'select',
                'title'  => Yii::t(
                    'TimelineModule.timeline',
                    'Select {field}',
                    ['{field}' => mb_strtolower($model->getAttributeLabel('doctor_id'))]
                ),
                'source' => CHtml::listData(TimelineDoctor::model()->findAll(), 'id', 'fullName'),
                'params' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                ]
            ],
            'name'     => 'doctor_id',
            'type'     => 'raw',
            'filter'   => CHtml::activeDropDownList(
                $model,
                'doctor_id',
                CHtml::listData(TimelineDoctor::model()->findAll(), 'id', 'fullName'),
                ['class' => 'form-control', 'empty' => '']
            ),
        ],
        [
            'class'    => 'bootstrap.widgets.TbEditableColumn',
            'editable' => [
                'url'    => $this->createUrl('/timeline/timelineEventBackend/inline'),
                'mode'   => 'popup',
                'type'   => 'select',
                'title'  => Yii::t(
                    'TimelineModule.timeline',
                    'Select {field}',
                    ['{field}' => mb_strtolower($model->getAttributeLabel('patient_id'))]
                ),
                'source' => CHtml::listData(User::model()->findAll(), 'id', 'fullName'),
                'params' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                ]
            ],
            'name'     => 'patient_id',
            'type'     => 'raw',
            'filter'   => CHtml::activeDropDownList(
                $model,
                'patient_id',
                CHtml::listData(User::model()->findAll(), 'id', 'fullName'),
                ['class' => 'form-control', 'empty' => '']
            ),
        ],
        'comment',
        'source',
        /*
        'visit',
        'description',
        'source',
        'status',
        'create_user_id',
        'update_user_id',
        'start_time',
        'end_time',
        'create_time',
        'update_time',
        */
        array(
            'class' => 'yupe\widgets\CustomButtonColumn',
        ),
    ),
)); ?>

<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TimelineEvent
 *   @var $form TbActiveForm
 *   @var $this TimelineEventBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'timeline-event-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'htmlOptions'            => array('class' => 'well'),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'afterValidate'=>'js:function(form,data,hasError){
                                    if(!hasError){
                                        $.ajax({
                                            type:"POST",
                                            url:"'.CHtml::normalizeUrl(array("TimelineEventBackend/create")).'",
                                            data:form.serialize(),
                                            dataType : "json",
                                            success: function(d,status){
                                                console.log(status);
                                                //console.log(d.status);

                                                /*if(d.status){
                                                    console.log("Равны");
                                                }*/
                                                //$("#patient-registration-modal").modal("hide");
                                                //console.log(d);
                                                //$("#calendar").fullCalendar( "addEventSource", [d] );
                                                $("#calendar").fullCalendar("refetchEvents");
                                                $("#add-event-modal").modal("hide");
                                            }
                                        });
                                    }
                                }'
        ),
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->hiddenField($model,'doctor_id');?>
<?php echo $form->hiddenField($model,'start_time');?>
<?php echo $form->hiddenField($model,'end_time');?>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->select2Group(
                $model,
                'patient_id',
                [
                    'widgetOptions' => [
                        'data' => ['' => '---'] + CHtml::listData(User::model()->getList(), 'id', 'fullName'),
                    ]
                ]
            ); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup(
                $model,
                'visit',
                [
                    'widgetOptions' => [
                        'data'        => $model->getVisitList(),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('visit'),
                            'data-content'        => $model->getAttributeDescription('visit')
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textAreaGroup($model, 'comment', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('comment'), 'data-content' => $model->getAttributeDescription('comment'))))); ?>
        </div>
    </div>

<?php if($model->scenario === 'insert'){?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('timeline', 'Сохранить Событие и продолжить'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('timeline', 'Сохранить Событие и закрыть'),
        )
    ); ?>
<?php }else{?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'update'),
            'label'      => Yii::t('timeline', 'Сохранить Событие'),
        )
    ); ?>
<?php }?>
<?php $this->endWidget(); ?>



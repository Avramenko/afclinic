<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$today = date("Y-m-d");
//$eventUrl = Yii::app()->createUrl($this->route);
$eventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/getEventJson');
$sourceUrl = Yii::app()->createUrl('/backend/timeline/timelineDoctor/getJson');
$userCreateUrl = Yii::app()->createUrl('/backend/user/user/create');
$viewEventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/view/');
$addEventUrl = Yii::app()->createUrl('/backend/timeline/timelineEvent/create');
$addPatientUrl = Yii::app()->createUrl('/backend/timeline/patient/create');


$moduleAssets = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.timeline.views.assets')
);

//Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/css/style.css');
Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/lib/fullcalendar.min.css');
//Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/lib/fullcalendar.print.css');
Yii::app()->getClientScript()->registerCssFile($moduleAssets . '/cal/scheduler.css');
//Yii::app()->getClientScript()->registerScriptFile($moduleAssets.'/js/script.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/moment.min.js',CClientScript::POS_HEAD);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/fullcalendar.min.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/scheduler.min.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScriptFile($moduleAssets . '/cal/lib/lang/ru.js',CClientScript::POS_END);
//Yii::app()->getClientScript()->registerScriptFile('//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/gcal.js',CClientScript::POS_END);
//Yii::app()->getClientScript()->registerScriptFile('//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/lang/ru.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScript('InitScheduler',
    <<<JS
    $(document).ready(function(){
        $('#calendar').fullCalendar({
            height: 'auto',
            slotDuration: '00:15:00',
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			now: '$today',
			selectable: true,
			//editable: true, // enable draggable events
			aspectRatio: 1.8,
			scrollTime: '10:00', // undo default 6am scrollTime
			minTime: '10:00:00',
            maxTime: '21:00:00',
			header: {
				left: 'addEvent, addUser, promptResource, today prev,next',
				center: 'title',
				//right: 'timelineDay,timelineThreeDays,agendaWeek,month'
				right: 'timelineDay,timelineWeek,month'
			},
			customButtons: {
			    addUser: {
                    text: 'Новый пациент',
                    click: function() {
                        $.ajax({
                                    type: "GET",
                                    url: "$addPatientUrl",
                                    dataType: "html",
                                    async : true,
                                    //data: $("#review-comment-personalInformation").serialize(),
                                    success: function(data){
                                        $("#patient-registration-modal .modal-body").html(data);
                                        $("#patient-registration-modal").modal('show');
                                    }
                                });
                        //$('#patient-registration-modal').modal('show');
                    }
                }/*,
                addEvent: {
                    text: 'записать на прием',
                    click: function() {
                        /!*var title = prompt('номер телефона');
                        if (title) {
                            $('#calendar').fullCalendar(
                                'addResource',
                                { title: title },
                                true // scroll to the new resource?
                            );
                        }*!/
                        $('#add-event-modal').modal('show');
                    }
                }*//*,
                promptResource: {
                    text: '+ кабинет',
                    click: function() {
                        var title = prompt('Описание кабинета');
                        if (title) {
                            $('#calendar').fullCalendar(
                                'addResource',
                                { title: title },
                                true // scroll to the new resource?
                            );
                        }
                    }
                }*/
            },
			defaultView: 'timelineWeek',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 3 }
				}
			},
			//timeFormat: 'h:mm',
			resourceLabelText: 'Врачи',

			resources: { // you can also specify a plain string like 'json/resources.json'
				url: '$sourceUrl',
				error: function(e) {
					console.log('error');
					console.log(e);
				}
			},

			events: {
			    url : '$eventUrl',
			    error: function(e) {
					console.log('error');
					console.log(e);
				}
			},
			select: function(start, end, jsEvent, view, resource) {
			    //console.log(view.name);
			    if(view.name === 'month'){
			        $('#calendar').fullCalendar( 'changeView', 'timelineDay' );
			        $('#calendar').fullCalendar( 'gotoDate', start.format() );
			    }
				console.log(
					'select callback',
					start.format(),
					end.format(),
					resource ? resource.id : '(no resource)'
				);
				if(view.name === 'timelineDay'){
                    //console.log('покажем форму для записи на прием');
                    $.ajax({
                        url : '$addEventUrl',
                        type : 'GET',
                        dataType : 'html',
                        data : {
                            TimelineEvent : {
                                start_time : start.format(),
                                end_time : end.format(),
                                doctor_id : resource.id
                            }
                        },
                        success : function( data, textStatus, jqXHR){
                            console.log('add event');
                            //console.log(data);
                            //console.log(textStatus);
                            //console.log(jqXHR);
                            $('#add-event-modal .modal-body').html(data);
                            $('#add-event-modal').modal('show');
                        },
                        error : function(jqXHR, status, errorThrown ){
                            console.log(status);
                            console.log(errorThrown);
                        }
                    });

			    }
			},
			dayClick: function() {
			    // отрабатывает при любом нажатии на рабочую область календаря
                //alert('a day has been clicked!');
            },
            eventClick: function(calEvent, jsEvent, view) {
                console.log(calEvent);
                console.log(jsEvent);
                console.log(view);
            //console.log(this);
                $.ajax({
                    url : '$viewEventUrl/'+calEvent.id,
                    type : 'GET',
                    dataType : 'html',
                    success : function( data, textStatus, jqXHR){
                        $('#view-event-modal .modal-body').html(data);
                        $('#view-event-modal').modal('show');
                    },
                    error : function(jqXHR, status, errorThrown ){
                        console.log(status);
                        console.log(errorThrown);
                    }
                });


                if (calEvent.url) {
                //console.log(calEvent);
                    //window.open(event.url);
                    return false;
                }
            },
            loading : function( isLoading, view ){
                console.log('isLoading: '+isLoading);
                console.log(view);
                if(isLoading){
                    $("#loading").addClass('loader in');
                }else{
                    $("#loading").removeClass('loader in');
                }
            }
		});

		/*$('#select-G').on('click', function() {
			$('#calendar').fullCalendar('select', '2015-08-07T02:00:00', '2015-08-07T07:00:00', 'g');
		});

		$('#select-unspecified').on('click', function() {
			$('#calendar').fullCalendar('select', '2015-08-07T02:00:00', '2015-08-07T07:00:00');
		});*/
		/*$.fn.popover({
                content : 'Привет!',
                placement : 'top'
        });*/
});
JS
    ,CClientScript::POS_END);


$this->breadcrumbs = array(
    Yii::app()->getModule('timeline')->getCategory() => array(),
    Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
    Yii::t('timeline', 'Shedule'),
);

$this->pageTitle = Yii::t('timeline', 'Shedule');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
);
?>

<?php
Yii::app()->getClientScript()->registerScript('GetPatientForm',
    <<<JS
    $(document).ready(function(){
    //$.fn.modal.Constructor.prototype.enforceFocus = function() {};
    /*$('#add-patient').on('click',function(){

        return false;
    });*/
});
JS
    ,CClientScript::POS_END)
?>
<?php
/*$row = 1;
if (($handle = fopen("test.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        echo "<p> $num полей в строке $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            echo $data[$c] . "<br />\n";
        }
    }
    fclose($handle);
}
*/?>
<div id="patient-registration-modal" class="modal fade" role="dialog" aria-labelledby="userModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="userModalLabel">Карточка Па</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="add-event-modal" class="modal fade" role="dialog" aria-labelledby="addEventModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addEventModalLabel">Add Event</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="view-event-modal" class="modal fade" role="dialog" aria-labelledby="viewEventModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="viewEventModalLabel">View Event</h4>
            </div>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Расписание'); ?>

    </h1>
</div>

<div id="calendar"></div>

<pre>
    <?php


    ;?>
</pre>
<!--<p style='text-align:center'>
    <button id='select-G'>select G</button>
    <button id='select-unspecified'>select w/o a resource</button>
</p>-->
<br/>

<?php
$this->widget('yupe\widgets\CustomGridView', array(
    'id'           => 'timeline-event-grid',
    'type'         => 'striped condensed',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'id',
        'start_time',
        [
            'class'    => 'bootstrap.widgets.TbEditableColumn',
            'editable' => [
                'url'    => $this->createUrl('/timeline/timelineEventBackend/inline'),
                'mode'   => 'popup',
                'type'   => 'select',
                'title'  => Yii::t(
                    'TimelineModule.timeline',
                    'Select {field}',
                    ['{field}' => mb_strtolower($model->getAttributeLabel('doctor_id'))]
                ),
                'source' => CHtml::listData(TimelineDoctor::model()->findAll(), 'id', 'fullName'),
                'params' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                ]
            ],
            'name'     => 'doctor_id',
            'type'     => 'raw',
            'filter'   => CHtml::activeDropDownList(
                $model,
                'doctor_id',
                CHtml::listData(TimelineDoctor::model()->findAll(), 'id', 'fullName'),
                ['class' => 'form-control', 'empty' => '']
            ),
        ],
        [
            'class'    => 'bootstrap.widgets.TbEditableColumn',
            'editable' => [
                'url'    => $this->createUrl('/timeline/timelineEventBackend/inline'),
                'mode'   => 'popup',
                'type'   => 'select',
                'title'  => Yii::t(
                    'TimelineModule.timeline',
                    'Select {field}',
                    ['{field}' => mb_strtolower($model->getAttributeLabel('patient_id'))]
                ),
                'source' => CHtml::listData(User::model()->findAll(), 'id', 'fullName'),
                'params' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                ]
            ],
            'name'     => 'patient_id',
            'type'     => 'raw',
            'filter'   => CHtml::activeDropDownList(
                $model,
                'patient_id',
                CHtml::listData(User::model()->findAll(), 'id', 'fullName'),
                ['class' => 'form-control', 'empty' => '']
            ),
        ],
        'comment',
        'source',
        /*
        'visit',
        'description',
        'source',
        'status',
        'create_user_id',
        'update_user_id',
        'start_time',
        'end_time',
        'create_time',
        'update_time',
        */
        array(
            'class' => 'yupe\widgets\CustomButtonColumn',
        ),
    ),
)); ?>

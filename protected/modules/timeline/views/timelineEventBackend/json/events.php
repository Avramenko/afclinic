<?php
header('Content-Type: application/json');
$json = [];
foreach($events as $event ){
    $json[] = [
        'id' => $event->id,
        'resourceId' => $event->doctor->id,
        'start' => $event->start_time,
        'end' => $event->end_time,
        'title' => $event->patient->getFullName(),
        //'className' => 'tooltip',
        'color' => $event->doctor->color,
        'constraint' => 'available',
    ];
}
echo CJSON::encode($json);
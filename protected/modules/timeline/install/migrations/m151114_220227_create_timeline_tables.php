<?php

class m151114_220227_create_timeline_tables extends yupe\components\DbMigration
{
	/*public function up()
	{
	}

	public function down()
	{
		echo "m151114_220227_create_timeline_tables does not support migration down.\n";
		return false;
	}*/


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{


        $this->createTable(
            '{{timeline_doctor}}',
            [
                'id' => 'pk ',
                'status' => 'int null',
                'user_id' => 'int not null',
                'position' => 'varchar(255) null',
                'description' => 'text null',
                'photo' => 'varchar(255) null',
                'color' => 'varchar(255) null',
                'create_time' => 'datetime DEFAULT NULL',
                'update_time' => 'datetime DEFAULT NULL',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
        $this->createIndex('idx_{{timeline_doctor}}_user_id', '{{timeline_doctor}}', 'user_id');
        $this->addForeignKey('fk_{{timeline_doctor}}_user', '{{timeline_doctor}}', 'user_id', '{{user_user}}', 'id', 'CASCADE', 'NO ACTION');
        /**
         * Таблица Timeline Schedule для хранения моделей событий в расписании
         */
        $this->createTable(
            '{{timeline_schedule}}',
            [
                'id' => 'pk',
                'doctor_id' => 'integer not null',
                'comment' => 'text null',
                'status' => 'int null',
                'create_user_id' => 'integer null',
                'update_user_id' => 'integer null',
                'start_time' => 'datetime DEFAULT NULL',
                'end_time' => 'datetime DEFAULT NULL',
                'create_time' => 'datetime DEFAULT NULL',
                'update_time' => 'datetime DEFAULT NULL',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
        $this->createIndex('idx_{{timeline_schedule}}_doctor_id', '{{timeline_schedule}}', 'doctor_id');
        $this->addForeignKey('fk_{{timeline_schedule}}_doctor', '{{timeline_schedule}}', 'doctor_id', '{{timeline_doctor}}', 'id', 'CASCADE', 'NO ACTION');
        /**
         * Таблица Timeline Event для хранения моделей событий в расписании
         */
        $this->createTable(
            '{{timeline_event}}',
            [
                'id' => 'pk',
                'doctor_id' => 'integer not null',
                'patient_id' => 'integer not null',
                'visit' => 'int null',
                'comment' => 'text null',
                'description' => 'text null',
                'source' => 'varchar(255) null',
                'status' => 'int null',
                'create_user_id' => 'integer null',
                'update_user_id' => 'integer null',
                'start_time' => 'datetime DEFAULT NULL',
                'end_time' => 'datetime DEFAULT NULL',
                'create_time' => 'datetime DEFAULT NULL',
                'update_time' => 'datetime DEFAULT NULL',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );


        /**
         * Таблица Timeline Message для хранения моделей сообщений отправленных пациентам
         */
        $this->createTable(
            '{{timeline_message}}',
            [
                'id' => 'pk',
                'type' => 'int not null',
                'mes' => 'text null',
                'sender' => 'varchar(255) null',
                'time' => 'datetime DEFAULT NULL',
                'query' => 'text null',
                'cost' => "decimal(19,3) not null default '0'",
                'cnt' => 'int null',
                'error' => 'int null',
                'err' => 'int null',
                'status' => 'int null',
                'last_date' => 'datetime DEFAULT NULL',
                'create_time' => 'datetime DEFAULT NULL',
                'update_time' => 'datetime DEFAULT NULL',
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );


        $this->createIndex('idx_{{timeline_event}}_doctor_id', '{{timeline_event}}', 'doctor_id');
        $this->addForeignKey('fk_{{timeline_event}}_doctor', '{{timeline_event}}', 'doctor_id', '{{timeline_doctor}}', 'id', 'CASCADE', 'NO ACTION');

        $this->createIndex('idx_{{timeline_event}}_patient_id', '{{timeline_event}}', 'patient_id');
        $this->addForeignKey('fk_{{timeline_event}}_patient', '{{timeline_event}}', 'patient_id', '{{user_user}}', 'id', 'CASCADE', 'NO ACTION');

        $this->createIndex('idx_{{timeline_event}}_create_user_id', '{{timeline_event}}', 'create_user_id');
        $this->addForeignKey('fk_{{timeline_event}}_create_user', '{{timeline_event}}', 'create_user_id', '{{user_user}}', 'id', 'CASCADE', 'NO ACTION');

        $this->createIndex('idx_{{timeline_event}}_update_user_id', '{{timeline_event}}', 'update_user_id');
        $this->addForeignKey('fk_{{timeline_event}}_update_user', '{{timeline_event}}', 'update_user_id', '{{user_user}}', 'id', 'CASCADE', 'NO ACTION');

        // post to tag
        $this->createTable(
            '{{timeline_event_to_message}}',
            [
                'event_id' => 'integer NOT NULL',
                'message_id'  => 'integer NOT NULL',
                'PRIMARY KEY (event_id, message_id)'
            ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );

        //ix
        $this->createIndex('ix_{{timeline_event_to_message}}_event_id', '{{timeline_event_to_message}}', 'event_id', false);
        $this->createIndex('ix_{{timeline_event_to_message}}_message_id', '{{timeline_event_to_message}}', 'message_id', false);

        //fk
        $this->addForeignKey(
            'fk_{{timeline_event_to_message}}_event_id',
            '{{timeline_event_to_message}}',
            'event_id',
            '{{timeline_event}}',
            'id',
            'CASCADE',
            'NO ACTION'
        );
        $this->addForeignKey(
            'fk_{{timeline_event_to_message}}_message_id',
            '{{timeline_event_to_message}}',
            'message_id',
            '{{timeline_message}}',
            'id',
            'CASCADE',
            'NO ACTION'
        );
    }

	public function safeDown()
	{
        $this->dropTableWithForeignKeys('{{timeline_event_to_message}}');
        $this->dropTableWithForeignKeys('{{timeline_message}}');
        $this->dropTableWithForeignKeys('{{timeline_event}}');
        $this->dropTableWithForeignKeys('{{timeline_schedule}}');
        $this->dropTableWithForeignKeys('{{timeline_doctor}}');
	}

}
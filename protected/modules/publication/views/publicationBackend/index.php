<?php

/**
 * @var $model Publication
 * @var $this PublicationBackendController
 */

$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publicationBackend/index'),
    Yii::t('PublicationModule.publication', 'Management'),
);

$this->pageTitle = Yii::t('PublicationModule.publication', 'Publication - management');

$this->menu = array(
		array (
				'icon' => 'fa fa-fw fa-list-alt',
				'label' => Yii::t ( 'PublicationModule.publication', 'Publication management' ),
				'url' => array (
						'/publication/publicationBackend/index' 
				) 
		),
		array (
				'icon' => 'fa fa-fw fa-list-alt',
				'label' => Yii::t ( 'PublicationModule.publication', 'Управление категориями' ),
				'url' => array (
						'/publication/publicationcatBackend/index' 
				) 
		),
		array (
				'icon' => 'fa fa-fw fa-plus-square',
				'label' => Yii::t ( 'PublicationModule.publication', 'Create article' ),
				'url' => array (
						'/publication/publicationBackend/create' 
				) 
		),
		array (
				'icon' => 'fa fa-fw fa-plus-square',
				'label' => Yii::t ( 'PublicationModule.publication', 'Create category' ),
				'url' => array (
						'/publication/publicationcatBackend/create' 
				) 
		),

);
?>
<div class="page-header">
	<h1>
        <?php echo Yii::t('PublicationModule.publication', 'Publication'); ?>
        <small><?php echo Yii::t('PublicationModule.publication', 'management'); ?></small>
	</h1>
</div>

<p>
	<a class="btn btn-default btn-sm dropdown-toggle"
		data-toggle="collapse" data-target="#search-toggle"> <i
		class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('PublicationModule.publication', 'Find publication'); ?>
        <span class="caret">&nbsp;</span>
	</a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('publication-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'publication-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'title',
                'editable' => array(
                    'url'    => $this->createUrl('/publication/publicationBackend/inline'),
                    'mode'   => 'inline',
                    'params' => array(
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    )
                ),
                'filter'   => CHtml::activeTextField($model, 'title', array('class' => 'form-control')),
            ),
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'alias',
                'editable' => array(
                    'url'    => $this->createUrl('/publication/publicationBackend/inline'),
                    'mode'   => 'inline',
                    'params' => array(
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    )
                ),
                'filter'   => CHtml::activeTextField($model, 'alias', array('class' => 'form-control')),
            ),
            'date',
            array(
                'name'   => 'category_id',
                'value'  => '$data->getCategoryName()',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'category_id',
                    Publicationcat::model()->getFormattedList(Yii::app()->getModule('publication')->mainCategory),
                    array('class' => 'form-control', 'encode' => false, 'empty' => '')
                )
            ),
            array(
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/publication/publicationBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Publication::STATUS_PUBLISHED  => ['class' => 'label-success'],
                    Publication::STATUS_MODERATION => ['class' => 'label-warning'],
                    Publication::STATUS_DRAFT      => ['class' => 'label-default'],
                ],
            ),
            array(
                'name'   => 'lang',
                'value'  => '$data->getFlag()',
                'filter' => $this->yupe->getLanguagesList(),
                'type'   => 'html'
            ),
            array(
                'class' => 'yupe\widgets\CustomButtonColumn'
            ),
        ),
    )
); ?>

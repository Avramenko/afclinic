<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publicationBackend/index'),
    Yii::t('PublicationModule.publication', 'Create'),
);

$this->pageTitle = Yii::t('PublicationModule.publication', 'Publication - create');

$this->menu = array(
    array(
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PublicationModule.publication', 'Publication management'),
        'url'   => array('/publication/publicationBackend/index')
    ),
    array(
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PublicationModule.publication', 'Create article'),
        'url'   => array('/publication/publicationBackend/create')
    ),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PublicationModule.publication', 'Publication'); ?>
        <small><?php echo Yii::t('PublicationModule.publication', 'create'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'languages' => $languages)); ?>

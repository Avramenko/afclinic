<?php
/**
 * Publication основная моделька для новостей
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.publication.models
 * @since 0.1
 *
 */

/**
 * This is the model class for table "Publication".
 *
 * The followings are the available columns in table 'Publication':
 * @property integer $id
 * @property string $creation_date
 * @property string $change_date
 * @property string $date
 * @property string $title
 * @property string $alias
 * @property string $short_text
 * @property string $full_text
 * @property integer $user_id
 * @property integer $status
 * @property integer $is_protected
 * @property string  $link
 * @property string  $image
 */
class Publication extends yupe\models\YModel
{

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_MODERATION = 2;

    const PROTECTED_NO = 0;
    const PROTECTED_YES = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{publication_publication}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return Publication   the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title, alias, short_text, full_text, keywords, description', 'filter', 'filter' => 'trim'),
            array('title, alias, keywords, description', 'filter', 'filter' => array(new CHtmlPurifier(), 'purify')),
            array('date, title, alias, full_text', 'required', 'on' => array('update', 'insert')),
            array('status, is_protected, category_id', 'numerical', 'integerOnly' => true),
            array('title, alias, keywords', 'length', 'max' => 150),
            array('lang', 'length', 'max' => 2),
            array('lang', 'default', 'value' => Yii::app()->sourceLanguage),
            array('lang', 'in', 'range' => array_keys(Yii::app()->getModule('yupe')->getLanguagesList())),
            array('status', 'in', 'range' => array_keys($this->getStatusList())),
            array('alias', 'yupe\components\validators\YUniqueSlugValidator'),
            array('description', 'length', 'max' => 250),
            array('link', 'length', 'max' => 250),
            array('link', 'yupe\components\validators\YUrlValidator'),
            array(
                'alias',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('PublicationModule.publication', 'Bad characters in {attribute} field')
            ),
            array('category_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array(
                'id, keywords, description, creation_date, change_date, date, title, alias, short_text, full_text, user_id, status, is_protected, lang',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('publication');

        return array(
            'imageUpload' => array(
                'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
                'scenarios'     => array('insert', 'update'),
                'attributeName' => 'image',
                'minSize'       => $module->minSize,
                'maxSize'       => $module->maxSize,
                'types'         => $module->allowedExtensions,
                'uploadPath'    => $module->uploadPath,
                'fileName'      => array($this, 'generateFileName'),
            ),
        );
    }

    public function generateFileName()
    {
        return md5($this->title . microtime(true) . uniqid());
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Publicationcat', 'category_id'),
            'user'     => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => 't.status = :status',
                'params'    => array(':status' => self::STATUS_PUBLISHED),
            ),
            'protected' => array(
                'condition' => 't.is_protected = :is_protected',
                'params'    => array(':is_protected' => self::PROTECTED_YES),
            ),
            'public'    => array(
                'condition' => 't.is_protected = :is_protected',
                'params'    => array(':is_protected' => self::PROTECTED_NO),
            ),
            'recent'    => array(
                'order' => 'creation_date DESC',
                'limit' => 5,
            )
        );
    }

    public function last($num)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'order' => 'date DESC',
                'limit' => $num,
            )
        );

        return $this;
    }

    public function language($lang)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'lang = :lang',
                'params'    => array(':lang' => $lang),
            )
        );

        return $this;
    }

    public function category($category_id)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'category_id = :category_id',
                'params'    => array(':category_id' => $category_id),
            )
        );

        return $this;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'            => Yii::t('PublicationModule.publication', 'Id'),
            'category_id'   => Yii::t('PublicationModule.publication', 'Category'),
            'creation_date' => Yii::t('PublicationModule.publication', 'Created at'),
            'change_date'   => Yii::t('PublicationModule.publication', 'Updated at'),
            'date'          => Yii::t('PublicationModule.publication', 'Date'),
            'title'         => Yii::t('PublicationModule.publication', 'Title'),
            'alias'         => Yii::t('PublicationModule.publication', 'Alias'),
            'image'         => Yii::t('PublicationModule.publication', 'Image'),
            'link'          => Yii::t('PublicationModule.publication', 'Link'),
            'lang'          => Yii::t('PublicationModule.publication', 'Language'),
            'short_text'    => Yii::t('PublicationModule.publication', 'Short text'),
            'full_text'     => Yii::t('PublicationModule.publication', 'Full text'),
            'user_id'       => Yii::t('PublicationModule.publication', 'Author'),
            'status'        => Yii::t('PublicationModule.publication', 'Status'),
            'is_protected'  => Yii::t('PublicationModule.publication', 'Access only for authorized'),
            'keywords'      => Yii::t('PublicationModule.publication', 'Keywords (SEO)'),
            'description'   => Yii::t('PublicationModule.publication', 'Description (SEO)'),
        );
    }

    public function beforeValidate()
    {
        if (!$this->alias) {
            $this->alias = yupe\helpers\YText::translit($this->title);
        }

        if (!$this->lang) {
            $this->lang = Yii::app()->getLanguage();
        }

        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        $this->change_date = new CDbExpression('NOW()');
        $this->date = date('Y-m-d', strtotime($this->date));

        if ($this->isNewRecord) {
            $this->creation_date = $this->change_date;
            $this->user_id = Yii::app()->getUser()->getId();
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->date = date('d-m-Y', strtotime($this->date));

        return parent::afterFind();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('creation_date', $this->creation_date, true);
        $criteria->compare('change_date', $this->change_date, true);
        if ($this->date) {
            $criteria->compare('date', date('Y-m-d', strtotime($this->date)));
        }
        $criteria->compare('title', $this->title, true);
        $criteria->compare('t.alias', $this->alias, true);
        $criteria->compare('short_text', $this->short_text, true);
        $criteria->compare('full_text', $this->full_text, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('category_id', $this->category_id, true);
        $criteria->compare('is_protected', $this->is_protected);
        $criteria->compare('t.lang', $this->lang);
        $criteria->with = array('category');

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort'     => array('defaultOrder' => 'date DESC'),
        ));
    }

    public function getPermaLink()
    {
        return Yii::app()->createAbsoluteUrl('/publication/publication/show/', array('alias' => $this->alias));
    }

    public function getStatusList()
    {
        return array(
            self::STATUS_DRAFT      => Yii::t('PublicationModule.publication', 'Draft'),
            self::STATUS_PUBLISHED  => Yii::t('PublicationModule.publication', 'Published'),
            self::STATUS_MODERATION => Yii::t('PublicationModule.publication', 'On moderation'),
        );
    }

    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('PublicationModule.publication', '*unknown*');
    }

    public function getProtectedStatusList()
    {
        return array(
            self::PROTECTED_NO  => Yii::t('PublicationModule.publication', 'no'),
            self::PROTECTED_YES => Yii::t('PublicationModule.publication', 'yes'),
        );
    }

    public function getProtectedStatus()
    {
        $data = $this->getProtectedStatusList();

        return isset($data[$this->is_protected]) ? $data[$this->is_protected] : Yii::t('PublicationModule.publication', '*unknown*');
    }

    public function getCategoryName()
    {
        return ($this->category === null) ? '---' : $this->category->name;
    }

    public function getFlag()
    {
        return yupe\helpers\YText::langToflag($this->lang);
    }
}

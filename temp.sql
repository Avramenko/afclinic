INSERT INTO `yupe_page_page`
(
  `id`,
  `parent_id`,
  `create_time`,
  `update_time`,
  `title_short`,
  `title`,
  `slug`,
  `body`,
  `keywords`,
  `description`,
  `status`,
  `order`
)
  SELECT
    `idItems`,
    `pKey`,
    NOW(),
    NOW(),
    `tName`,
    `tTitle`,
    `tAlias`,
    `tText`,
    `tMetaKey`,
    `tMetaDesc`,
    '1',
    `nOrder`
  FROM `jexkit_items`;

UPDATE `yupe_page_page`
SET
  `layout` = 'main',
  `lang` = 'ru',
  `view` = '',
  `category_id` = '1',
  `user_id` = '1',
  `change_user_id` = '1';